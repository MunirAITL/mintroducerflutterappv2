import 'dart:async';

import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:flutter/services.dart';

class IncomingChannel {
  //isFire = true;
  final StreamController<String> _stringStreamController =
      StreamController<String>();

  Stream<String> get stringStream => _stringStreamController.stream;

  IncomingChannel() {
    // Set method call handler before telling platform side we are ready to receive.
    final methodChannel = const MethodChannel("flutter.native/incoming");
    methodChannel.setMethodCallHandler((call) async {
      print('Just received ${call.method} from platform');
      if (call.method == "invokeFCMToken") {
        await PrefMgr.shared
            .setPrefStr("fcmTokenKey", call.arguments as String);
      } else if (call.method == "invokeFCMRemoteMessageBody") {
        _stringStreamController.add(call.arguments as String);
      } else {
        print("Method not implemented: ${call.method}");
      }
    });
    //methodChannel.invokeMethod('wakeupFlutter');
  }

  /*static const channelName = 'flutter.native/mm4';
  MethodChannel methodChannel;

  static final FlutterMethodChannel instance = FlutterMethodChannel._init();
  FlutterMethodChannel._init();

  void configureChannel() {
    try {
      methodChannel = MethodChannel(channelName);
      methodChannel.setMethodCallHandler(methodHandler); // set method handler
    } catch (e) {
      print(e);
    }
  }

  Future<void> methodHandler(MethodCall call) async {
    final String idea = call.arguments;
    switch (call.method) {
      case "invokeFlutter": // this method name needs to be the same from invokeMethod in Android
        //DataService.instance.addIdea(idea); // you can handle the data here. In this example, we will simply update the view via a data service
        //print(idea);
        StateProvider().notifyWithData(ObserverState.STATE_HREF_LOGIN, idea);
        break;
      default:
        print('no method handler for method ${call.method}');
    }
  }*/
}
