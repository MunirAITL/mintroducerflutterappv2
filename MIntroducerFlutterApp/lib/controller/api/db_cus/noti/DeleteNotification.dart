class DeleteNotificationModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ErrorMessages responseData;

  DeleteNotificationModel({this.success, this.errorMessages, this.messages, this.responseData});

  DeleteNotificationModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null ? new ErrorMessages.fromJson(json['ErrorMessages']) : null;
    messages = json['Messages'] != null ? new ErrorMessages.fromJson(json['Messages']) : null;
    responseData = json['ResponseData'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    data['ResponseData'] = this.responseData;
    return data;
  }
}

class ErrorMessages {


  ErrorMessages();

ErrorMessages.fromJson(Map<String, dynamic> json) {
}

Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  return data;
}
}