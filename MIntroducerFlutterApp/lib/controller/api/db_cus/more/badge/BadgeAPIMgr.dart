import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/db_cus/tab_more/badge/BadgeEmailHelper.dart';
import 'package:aitl/controller/helper/db_cus/tab_more/badge/BadgePhotoIDHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgeAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgeEmailAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgePhotoIDAPIModel.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

class BadgeAPIMgr with Mixin {
  static final BadgeAPIMgr _shared = BadgeAPIMgr._internal();

  factory BadgeAPIMgr() {
    return _shared;
  }

  BadgeAPIMgr._internal();

  wsGetUserBadge(
      {BuildContext context,
      int userId,
      Function(BadgeAPIModel) callback}) async {
    try {
      debugPrint(
          "badge api = ${Server.BADGE_USER_GET_URL.replaceAll("#userId#", userId.toString())}");
      await NetworkMgr()
          .req<BadgeAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url:
            Server.BADGE_USER_GET_URL.replaceAll("#userId#", userId.toString()),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }

  wsPostEmailBadge(
      {BuildContext context,
      Function(BadgeEmailAPIModel model) callback}) async {
    try {
      await NetworkMgr()
          .req<BadgeEmailAPIModel, Null>(
        context: context,
        url: Server.BADGE_EMAIL_URL,
        param: BadgeEmailHelper().getParam(),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }

  wsPostPhotoIDBadge(
      {BuildContext context,
      var params,
      Function(BadgePhotoIDAPIModel model) callback}) async {
    try {
      await NetworkMgr()
          .req<BadgePhotoIDAPIModel, Null>(
        context: context,
        url: Server.BADGE_PHOTOID_URL,
        param: params,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }

  wsDelPhotoIDBadge(
      {BuildContext context,
      int id,
      Function(BadgePhotoIDAPIModel model) callback}) async {
    try {
      await NetworkMgr().req<BadgePhotoIDAPIModel, Null>(
        context: context,
        url:
            Server.BADGE_PHOTOID_DEL_URL.replaceAll("#badgeId#", id.toString()),
        reqType: ReqType.Delete,
        param: {},
      ).then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
