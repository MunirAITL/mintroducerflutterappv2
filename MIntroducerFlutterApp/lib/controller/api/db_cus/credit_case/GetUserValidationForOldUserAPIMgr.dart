import 'package:aitl/Mixin.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/db_cus/credit_report/CreditUrlHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDertailsAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/GetUserValidationOldUserApiModel.dart';
import 'package:flutter/cupertino.dart';

class GetUserValidationOldUserApiMgr with Mixin {
  static final GetUserValidationOldUserApiMgr _shared =
      GetUserValidationOldUserApiMgr._internal();

  factory GetUserValidationOldUserApiMgr() {
    return _shared;
  }

  GetUserValidationOldUserApiMgr._internal();

  creditCaseOldUserInformation({
    BuildContext context,
    int userID,
    int userCompanyId,
    Function(GetUserValidationOldUserApiModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<GetUserValidationOldUserApiModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: CreditURLbHelper()
            .getUrl(userCompanyId: userCompanyId, userID: userID),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog("GET_USER_VALIDATION_FOR_OLD_USER_URL  ERROR = " + e.toString());
    }
  }
}
