import 'package:aitl/config/Server.dart';
import 'package:aitl/config/ServerIntr.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/tab_more/support/ResolutionAPIModel.dart';
import 'package:flutter/material.dart';
import 'package:aitl/model/json/db_cus/case_lead/CaseLeadNegotiatorUserAPIModel.dart';
import 'package:aitl/Mixin.dart';

class CaseLeadNavigatorAPIMgr with Mixin {
  static final CaseLeadNavigatorAPIMgr _shared =
      CaseLeadNavigatorAPIMgr._internal();

  factory CaseLeadNavigatorAPIMgr() {
    return _shared;
  }

  CaseLeadNavigatorAPIMgr._internal();

  wsGetCaseLeadNavigatorByIntroducerID(
      {BuildContext context,
      // UserNotesModel userNotesModel,
      Function(CaseLeadNegotiatorUserAPIModel) callback}) async {
    try {
      await NetworkMgr()
          .req<CaseLeadNegotiatorUserAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: ServerIntr.CASELEAD_NAVIGATOR_GET_URL
            .replaceAll("#introducerId#", userData.userModel.id.toString()),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }

  wsPostResolutionAPI({
    BuildContext context,
    dynamic param,
    Function(ResolutionAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<ResolutionAPIModel, Null>(
              context: context,
              url: ServerIntr.POST_RESOLUTION_URL,
              param: param,
              reqType: ReqType.Post)
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }

  wsPutResolutionAPI({
    BuildContext context,
    dynamic param,
    Function(ResolutionAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<ResolutionAPIModel, Null>(
              context: context,
              url: ServerIntr.PUT_RESOLUTION_URL,
              param: param,
              reqType: ReqType.Put)
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
