import 'package:aitl/model/data/UserData.dart';
import 'package:jiffy/jiffy.dart';
import '../../../../model/json/db_intr/createlead/ResolutionModel.dart';
import '../../../../model/json/db_intr/leadfetch/LeadecaseApiModel.dart';

class CaseLeadNegotiatorHelper {
  postParam({
    int id = 0,
    int status = 101,
    int negotiatorId = 0,
    int groupId = 0,
    String caseType,
    String title,
    String fname,
    String lname,
    String mname,
    String mobile,
    String email,
    String note,
    String dob,
    String leadReferenceId,
    String leadAddr = '',
    String leadMortgageLengthLeft = '',
    String leadHomeValue = '',
    String leadMortgagePurpose = '',
    String leadMortgageAmount = '',
    String leadMortgageBorrowLength = '',
    String leadCreditRating = '',
    String leadEmploymentStatus = '',
    String leadYearlyIncome = '',
    String leadEmailConsent = "No",
    String estimatedEarning = "0",
    String areYouChargingAFee = 'No',
    String chargeFeeAmount = '0',
    String chargingFeeWhenPayable = '',
    String chargingFeeRefundable = 'No',
    String areYouChargingAnotherFee = 'No',
    String chargeAnotherFeeAmount = '',
    String chargingAnotherFeeWhenPayable = '',
    String chargingAnotherFeeRefundable = 'No',
    int caseOwnerId = 0,
    String caseOwnerName = '',
    ResolutionModel resolution,
  }) {
    var dd = '', mm = '', yy = '';
    try {
      final arr = dob.split("-");
      dd = arr[0];
      mm = arr[1];
      yy = arr[2];
    } catch (e) {}
    return {
      "Id": id,
      "Status": status,
      "GroupId": groupId,
      "Title": caseType == 'Others' ? title : caseType,
      "Description": caseType != 'Others' ? title : '',
      "Remarks": resolution != null ? resolution.remarks : '',
      "InitiatorId": userData.userModel.id,
      "ServiceDate": DateTime.now().toString(),
      "ResolutionType": resolution != null
          ? resolution.resolutionType
          : 'Lead From Introducer',
      "ParentId": resolution != null ? resolution.parentId ?? 0 : 0,
      "AssigneeId": caseOwnerId,
      "AssigneeName": caseOwnerName,
      "UserCompanyId": userData.userModel.userCompanyID,
      "LeadStatus": resolution != null ? resolution.leadStatus : '',
      "FirstName": fname.trim(),
      "MiddleName": mname.trim(),
      "LastName": lname.trim(),
      "MobileNumber": mobile.trim(),
      "Email": email.trim(),
      "DateofBirth": dob,
      "DateofBirthDD": dd,
      "DateofBirthMM": mm,
      "DateofBirthYY": yy,
      "ReferenceId": leadReferenceId == ''
          ? '0'
          : resolution != null
              ? resolution.leadReferenceId
              : '0',
      "Stage": resolution != null ? resolution.stage : '',
      "Probability": resolution != null ? resolution.probability ?? 0 : 0,
      "TitleOthers": title,
      "FileUrl": resolution != null ? resolution.fileUrl : '',
      "Address": leadAddr,
      "MortgageLengthLeft": leadMortgageLengthLeft,
      "HomeValue": leadHomeValue,
      "MortgagePurpose": leadMortgagePurpose,
      "MortgageAmount": leadMortgageAmount,
      "MortgageBorrowLength": leadMortgageBorrowLength,
      "CreditRating": leadCreditRating,
      "EmploymentStatus": leadEmploymentStatus,
      "YearlyIncome": leadYearlyIncome,
      "EmailConsent": leadEmailConsent,
      "EstimatedEarning": estimatedEarning == '' ? 0 : estimatedEarning,
      "Qualifier": resolution != null ? resolution.qualifier : '',
      "IsLockAutoCall": resolution != null ? resolution.isLockAutoCall ?? 0 : 0,
      "SupportAdminId": resolution != null ? resolution.supportAdminId ?? 0 : 0,
      "UserNoteEntityId":
          resolution != null ? resolution.userNoteEntityId ?? 0 : 0,
      "UserNoteEntityName":
          resolution != null ? resolution.userNoteEntityName : '',
      "NegotiatorId": negotiatorId,
      "PhoneNumber": mobile,
      "EmailAddress": email,
      "AreYouChargingAFee": areYouChargingAFee,
      "ChargeFeeAmount": chargeFeeAmount == '' ? 0 : chargeFeeAmount,
      "ChargingFeeWhenPayable": chargingFeeWhenPayable.startsWith("Select")
          ? ''
          : chargingFeeWhenPayable,
      "ChargingFeeRefundable": chargingFeeRefundable,
      "AreYouChargingAnotherFee": areYouChargingAnotherFee,
      "ChargeAnotherFeeAmount":
          chargeAnotherFeeAmount == '' ? 0 : chargeAnotherFeeAmount,
      "ChargingAnotherFeeWhenPayable":
          chargingAnotherFeeWhenPayable.startsWith("Select")
              ? ''
              : chargingAnotherFeeWhenPayable,
      "ChargingAnotherFeeRefundable": chargingAnotherFeeRefundable,
      "LeadNote": note,
      "ConsultantId": caseOwnerId,
      "ConsultantName": caseOwnerName,
    };
  }

  putParam({
    int status = 101,
    int negotiatorId = 0,
    int groupId = 0,
    String caseType,
    String title,
    String fname,
    String lname,
    String mname,
    String mobile,
    String email,
    String note,
    String dob,
    String leadReferenceId,
    String leadAddr = '',
    String leadMortgageLengthLeft = '',
    String leadHomeValue = '',
    String leadMortgagePurpose = '',
    String leadMortgageAmount = '',
    String leadMortgageBorrowLength = '',
    String leadCreditRating = '',
    String leadEmploymentStatus = '',
    String leadYearlyIncome = '',
    String leadEmailConsent = "No",
    String estimatedEarning = "0",
    String areYouChargingAFee = 'No',
    String chargeFeeAmount = "0",
    String chargingFeeWhenPayable = '',
    String chargingFeeRefundable = 'No',
    String areYouChargingAnotherFee = 'No',
    String chargeAnotherFeeAmount = "0",
    String chargingAnotherFeeWhenPayable = '',
    String chargingAnotherFeeRefundable = 'No',
    int caseOwnerId = 0,
    String caseOwnerName = '',
    ResolutionModel resolution,
  }) {
    var dd = '', mm = '', yy = '';
    try {
      final arr = dob.split("-");
      dd = arr[0];
      mm = arr[1];
      yy = arr[2];
    } catch (e) {}
    return {
      "UserId": userData.userModel.id,
      "CreationDate": DateTime.now().toString(),
      "Status": 101,
      "Title": caseType == 'Others' ? title : caseType,
      "Description": caseType != 'Others' ? title : '',
      "Remarks": resolution.remarks ?? '',
      "EmailAddress": email,
      "PhoneNumber": mobile,
      "InitiatorId": userData.userModel.id,
      "ServiceDate": DateTime.now().toString(),
      "ResolutionType": resolution != null
          ? resolution.resolutionType
          : 'Lead From Introducer',
      "ParentId": resolution.parentId ?? 0,
      "ProfileImageUrl": resolution.profileImageUrl ?? '',
      "ProfileOwnerName": resolution.profileOwnerName ?? '',
      "Complainee": resolution.complainee,
      "ComplaineeUrl": resolution.complaineeUrl,
      "AssigneeId": caseOwnerId,
      "AssigneeName": caseOwnerName,
      "AssignDate": resolution.assignDate ?? DateTime.now().toString(),
      "ResolvedDate": resolution.resolvedDate ?? DateTime.now().toString(),
      "TaskStatus": resolution.taskStatus,
      "UserComunity": userData.userModel.community,
      "ResolutionsUrl": resolution.resolutionsUrl,
      "UserCompanyId": userData.userModel.userCompanyID,
      "LeadStatus": resolution.leadStatus,
      "LeadNote": note,
      "FirstName": fname,
      "MiddleName": mname,
      "NamePrefix": resolution.namePrefix,
      "LastName": lname,
      "InitiatorImageUrl": resolution.initiatorImageUrl,
      "InitiatorName": resolution.initiatorName,
      "InitiatorCompanyName": resolution.initiatorCompanyName,
      "Stage": resolution.stage,
      "Probability": resolution.probability ?? 0,
      "CompanyName": resolution.companyName,
      "FileUrl": resolution.fileUrl,
      "LeadReferenceId": leadReferenceId == '' ? 0 : resolution.leadReferenceId,
      "Address": leadAddr,
      "MortgageLengthLeft": leadMortgageLengthLeft,
      "HomeValue": leadHomeValue,
      "MortgagePurpose": leadMortgagePurpose,
      "MortgageAmount": leadMortgageAmount,
      "MortgageBorrowLength": leadMortgageBorrowLength,
      "CreditRating": leadCreditRating,
      "EmploymentStatus": leadEmploymentStatus,
      "YearlyIncome": leadYearlyIncome,
      "EmailConsent": leadEmailConsent,
      "EstimatedEarning": estimatedEarning != '' ? estimatedEarning : '0',
      "Qualifier": resolution.qualifier ?? '',
      "IsLockAutoCall": resolution.isLockAutoCall ?? 0,
      "SupportAdminId": resolution.supportAdminId ?? 0,
      "UserNoteEntityId": resolution.userNoteEntityId ?? 0,
      "UserNoteEntityName": resolution.userNoteEntityName ?? '',
      "NegotiatorId": negotiatorId,
      "AreYouChargingAFee": areYouChargingAFee,
      "ChargeFeeAmount": chargeFeeAmount != '' ? chargeFeeAmount : '0',
      "ChargingFeeWhenPayable": chargingFeeWhenPayable != null
          ? chargingFeeWhenPayable.startsWith("Select")
              ? ''
              : chargingFeeWhenPayable
          : '',
      "ChargingFeeRefundable": chargingFeeRefundable,
      "AreYouChargingAnotherFee": areYouChargingAnotherFee,
      "ChargeAnotherFeeAmount":
          chargeAnotherFeeAmount != '' ? chargeAnotherFeeAmount : 0,
      "ChargingAnotherFeeWhenPayable": chargingAnotherFeeWhenPayable != null
          ? chargingAnotherFeeWhenPayable != ''
              ? chargingAnotherFeeWhenPayable.startsWith("Select")
                  ? ''
                  : chargingAnotherFeeWhenPayable
              : ''
          : '',
      "ChargingAnotherFeeRefundable": chargingAnotherFeeRefundable,
      "Id": resolution.id,
      "DateOfBirth": dob,
      "DateofBirthDD": dd,
      "DateofBirthMM": mm,
      "DateofBirthYY": yy,
      "TitleOthers": title,
      "ConsultantId": caseOwnerId,
      "ConsultantName": caseOwnerName,
    };
  }
}
