//Singleton reusable class
//  https://medium.com/@nshansundar/simple-observer-pattern-to-notify-changes-across-screens-in-flutter-dart-f035dbd990a9

abstract class StateListenerIntro {
  void onStateChanged(ObserverStateIntro state);
  void onStateChangedWithData(ObserverStateIntro state, data) {}
}

enum ObserverStateIntro {
  STATE_CHANGED_tabbar1_reload_case_api,
  STATE_CHANGED_tabbar2_reload_case_api,
  STATE_CHANGED_tabbar3_reload_case_api,
  STATE_CHANGED_tabbar4_reload_case_api,
  STATE_CHANGED_tabbar1,
  STATE_CHANGED_tabbar2,
  STATE_CHANGED_tabbar3,
  STATE_CHANGED_tabbar4,
  STATE_CHANGED_tabbar5,
  STATE_CHANGED_tabbar6,
  STATE_CHANGED_logout,
  STATE_CHANGED_full_report_credit_report,
  STATE_WEBVIEW_BACK,
  STATE_HREF_LOGIN,
}

class StateProviderIntro {
  List<StateListenerIntro> observers = [];
  static final StateProviderIntro _instance = new StateProviderIntro.internal();
  factory StateProviderIntro() => _instance;
  StateProviderIntro.internal() {
    //observers = new List<StateListenerIntro>();
    initState();
  }
  void initState() async {}

  void subscribe(StateListenerIntro listener) {
    observers.add(listener);
  }

  void unsubscribe(StateListenerIntro listener) {
    observers.remove(listener);
  }

  void notify(dynamic state) {
    observers.forEach((StateListenerIntro obj) => obj.onStateChanged(state));
  }

  void notifyWithData(dynamic state, dynamic data) {
    observers.forEach(
        (StateListenerIntro obj) => obj.onStateChangedWithData(state, data));
  }

  void dispose(StateListenerIntro thisObserver) {
    for (var obj in observers) {
      if (obj == thisObserver) {
        observers.remove(obj);
      }
    }
  }
}
