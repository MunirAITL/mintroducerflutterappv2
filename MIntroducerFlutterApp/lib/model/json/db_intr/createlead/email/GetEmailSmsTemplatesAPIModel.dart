class GetEmailSmsTemplatesAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetEmailSmsTemplatesAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetEmailSmsTemplatesAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<dynamic> emailAndSMSTemplates;
  ResponseData({this.emailAndSMSTemplates});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['EmailAndSMSTemplates'] != null) {
      emailAndSMSTemplates = <EmailAndSMSTemplates>[];
      json['EmailAndSMSTemplates'].forEach((v) {
        emailAndSMSTemplates.add(new EmailAndSMSTemplates.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.emailAndSMSTemplates != null) {
      data['EmailAndSMSTemplates'] =
          this.emailAndSMSTemplates.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class EmailAndSMSTemplates {
  int userId;
  int userCompanyId;
  int status;
  String creationDate;
  String type;
  String category;
  String title;
  String subject;
  String message;
  String remarks;
  String webURL;
  int id;

  EmailAndSMSTemplates(
      {this.userId,
      this.userCompanyId,
      this.status,
      this.creationDate,
      this.type,
      this.category,
      this.title,
      this.subject,
      this.message,
      this.remarks,
      this.webURL,
      this.id});

  EmailAndSMSTemplates.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    userCompanyId = json['UserCompanyId'];
    status = json['Status'];
    creationDate = json['CreationDate'];
    type = json['Type'];
    category = json['Category'];
    title = json['Title'];
    subject = json['Subject'];
    message = json['Message'];
    remarks = json['Remarks'];
    webURL = json['WebURL'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['UserCompanyId'] = this.userCompanyId;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['Type'] = this.type;
    data['Category'] = this.category;
    data['Title'] = this.title;
    data['Subject'] = this.subject;
    data['Message'] = this.message;
    data['Remarks'] = this.remarks;
    data['WebURL'] = this.webURL;
    data['Id'] = this.id;
    return data;
  }
}
