class GetForeCasts4PersonalAccountsAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetForeCasts4PersonalAccountsAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetForeCasts4PersonalAccountsAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  GetForeCasts4AccResponseData responseData;
  ResponseData({this.responseData});
  ResponseData.fromJson(Map<String, dynamic> json) {
    responseData = json['ResponseData'] != null
        ? new GetForeCasts4AccResponseData.fromJson(json['ResponseData'])
        : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (responseData != null) {
      data['ResponseData'] = responseData.toJson();
    }
    return data;
  }
}

class GetForeCasts4AccResponseData {
  FinancialCredibilityScore financialCredibilityScore;
  Affordability affordability;
  List<Liquidity> liquidity;
  List<FinancialForecast> financialForecast;

  GetForeCasts4AccResponseData(
      {this.financialCredibilityScore,
      this.affordability,
      this.liquidity,
      this.financialForecast});

  GetForeCasts4AccResponseData.fromJson(Map<String, dynamic> json) {
    financialCredibilityScore = json['financial_credibility_score'] != null
        ? new FinancialCredibilityScore.fromJson(
            json['financial_credibility_score'])
        : null;
    affordability = json['affordability'] != null
        ? new Affordability.fromJson(json['affordability'])
        : null;
    if (json['liquidity'] != null) {
      liquidity = [];
      json['liquidity'].forEach((v) {
        liquidity.add(new Liquidity.fromJson(v));
      });
    }
    if (json['financial_forecast'] != null) {
      financialForecast = [];
      json['financial_forecast'].forEach((v) {
        financialForecast.add(new FinancialForecast.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.financialCredibilityScore != null) {
      data['financial_credibility_score'] =
          this.financialCredibilityScore.toJson();
    }
    if (this.affordability != null) {
      data['affordability'] = this.affordability.toJson();
    }
    if (this.liquidity != null) {
      data['liquidity'] = this.liquidity.map((v) => v.toJson()).toList();
    }
    if (this.financialForecast != null) {
      data['financial_forecast'] =
          this.financialForecast.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class FinancialCredibilityScore {
  Generic generic;
  Custom custom;

  FinancialCredibilityScore({this.generic, this.custom});

  FinancialCredibilityScore.fromJson(Map<String, dynamic> json) {
    generic =
        json['generic'] != null ? new Generic.fromJson(json['generic']) : null;
    custom =
        json['custom'] != null ? new Custom.fromJson(json['custom']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.generic != null) {
      data['generic'] = this.generic.toJson();
    }
    if (this.custom != null) {
      data['custom'] = this.custom.toJson();
    }
    return data;
  }
}

class Generic {
  int score;
  String band;
  List<ReasonCodes> reasonCodes;

  Generic({this.score, this.band, this.reasonCodes});

  Generic.fromJson(Map<String, dynamic> json) {
    score = json['score'];
    band = json['band'];
    if (json['reason_codes'] != null) {
      reasonCodes = [];
      json['reason_codes'].forEach((v) {
        reasonCodes.add(new ReasonCodes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['score'] = this.score;
    data['band'] = this.band;
    if (this.reasonCodes != null) {
      data['reason_codes'] = this.reasonCodes.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ReasonCodes {
  String code;
  String description;
  String sentiment;

  ReasonCodes({this.code, this.description, this.sentiment});

  ReasonCodes.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    description = json['description'];
    sentiment = json['sentiment'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['description'] = this.description;
    data['sentiment'] = this.sentiment;
    return data;
  }
}

class Custom {
  int score;
  String band;
  List<ReasonCodes> reasonCodes;

  Custom({this.score, this.band, this.reasonCodes});

  Custom.fromJson(Map<String, dynamic> json) {
    score = json['score'];
    band = json['band'];
    if (json['reason_codes'] != null) {
      reasonCodes = [];
      json['reason_codes'].forEach((v) {
        reasonCodes.add(new ReasonCodes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['score'] = this.score;
    data['band'] = this.band;
    if (this.reasonCodes != null) {
      data['reason_codes'] = this.reasonCodes.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Affordability {
  int score;
  String band;

  Affordability({this.score, this.band});

  Affordability.fromJson(Map<String, dynamic> json) {
    score = json['score'];
    band = json['band'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['score'] = this.score;
    data['band'] = this.band;
    return data;
  }
}

class Liquidity {
  int score;
  String band;
  int days;

  Liquidity({this.score, this.band, this.days});

  Liquidity.fromJson(Map<String, dynamic> json) {
    score = json['score'];
    band = json['band'];
    days = json['days'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['score'] = this.score;
    data['band'] = this.band;
    data['days'] = this.days;
    return data;
  }
}

class FinancialForecast {
  Bank bank;
  FinancialForecast({this.bank});
  FinancialForecast.fromJson(Map<String, dynamic> json) {
    bank = json['bank'] != null ? new Bank.fromJson(json['bank']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.bank != null) {
      data['bank'] = this.bank.toJson();
    }
    return data;
  }
}

class Bank {
  String name;
  String slug;
  bool isActive;
  List<Account> account;

  Bank({this.name, this.slug, this.isActive, this.account});

  Bank.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    slug = json['slug'];
    isActive = json['is_active'];
    if (json['account'] != null) {
      account = [];
      json['account'].forEach((v) {
        account.add(new Account.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['slug'] = this.slug;
    data['is_active'] = this.isActive;
    if (this.account != null) {
      data['account'] = this.account.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Account {
  int id;
  String currency;
  String type;
  String number;
  String sortCodeAndAccountNumber;
  Balance balance;

  Account(
      {this.id,
      this.currency,
      this.type,
      this.number,
      this.sortCodeAndAccountNumber,
      this.balance});

  Account.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    currency = json['currency'];
    type = json['type'];
    number = json['number'];
    sortCodeAndAccountNumber = json['sort_code_and_account_number'];
    balance =
        json['balance'] != null ? new Balance.fromJson(json['balance']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['currency'] = this.currency;
    data['type'] = this.type;
    data['number'] = this.number;
    data['sort_code_and_account_number'] = this.sortCodeAndAccountNumber;
    if (this.balance != null) {
      data['balance'] = this.balance.toJson();
    }
    return data;
  }
}

class Balance {
  List<Actual> actual;
  List<Forecast> forecast;

  Balance({this.actual, this.forecast});

  Balance.fromJson(Map<String, dynamic> json) {
    if (json['actual'] != null) {
      actual = [];
      json['actual'].forEach((v) {
        actual.add(new Actual.fromJson(v));
      });
    }
    if (json['forecast'] != null) {
      forecast = [];
      json['forecast'].forEach((v) {
        forecast.add(new Forecast.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.actual != null) {
      data['actual'] = this.actual.map((v) => v.toJson()).toList();
    }
    if (this.forecast != null) {
      data['forecast'] = this.forecast.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Actual {
  double amount;
  int date;

  Actual({this.amount, this.date});

  Actual.fromJson(Map<String, dynamic> json) {
    amount = json['amount'];
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amount'] = this.amount;
    data['date'] = this.date;
    return data;
  }
}

class Forecast {
  double amount;
  int date;

  Forecast({this.amount, this.date});

  Forecast.fromJson(Map<String, dynamic> json) {
    amount = json['amount'];
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amount'] = this.amount;
    data['date'] = this.date;
    return data;
  }
}
