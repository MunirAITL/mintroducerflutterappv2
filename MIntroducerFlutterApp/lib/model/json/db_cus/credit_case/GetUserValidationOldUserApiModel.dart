import 'package:aitl/model/json/db_cus/credit_case/GetUserValidationOldUserResponse.dart';


class GetUserValidationOldUserApiModel {
  bool success;
  dynamic errorMessages;
  dynamic messages;
  GetUserValidationOldUser responseData;

  GetUserValidationOldUserApiModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory GetUserValidationOldUserApiModel.fromJson(Map<String, dynamic> j) {
    return GetUserValidationOldUserApiModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},
      responseData: (j['ResponseData'] != null)
          ? GetUserValidationOldUser.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}
