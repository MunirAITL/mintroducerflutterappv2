import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';

import '../../../widgets/txt/Txt.dart';

CaseTypeList(
    BuildContext context,
    ScrollController controller,
    String caseType,
    Function(String caseTypeTxtListList, Map<String, dynamic> caseTypeMap,
            List<Map<String, dynamic>> subTypeItemList)
        onClick) {
  final listCaseType = caseType.split(",");
  return Padding(
    padding: const EdgeInsets.only(top: 5),
    child: Container(
        child: ListView.builder(
      shrinkWrap: true,
      primary: false,
      scrollDirection: Axis.vertical,
      itemCount: listCaseType.length,
      itemBuilder: (context, index) {
        final name = listCaseType[index].trim();
        final map = NewCaseCfg().getCaseType(name);
        if (map == null) return SizedBox();
        final icon = map["url"];
        final title = map["title"];
        final mapSubList = map["subItem"];
        return GestureDetector(
          onTap: () {
            onClick(title, NewCaseCfg.listCreateNewCase[index], mapSubList);
          },
          child: Card(
            color: Colors.grey[300],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(width: 5),
                  Flexible(
                    child: Container(
                      width: 40,
                      height: 40,
                      child: Image.asset(
                        icon,
                        color: MyTheme.titleColor,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 5,
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Txt(
                          txt: title,
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.left,
                          maxLines: 4,
                          isBold: false,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    )),
  );
}
