import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';

import '../../../widgets/txt/Txt.dart';

CaseSubTypeList(BuildContext context, caseSubTypeText,
    Function(String subCaseType) onClick) {
  return Padding(
    padding: const EdgeInsets.only(top: 5),
    child: Container(
        child: ListView.builder(
      shrinkWrap: true,
      primary: false,
      scrollDirection: Axis.vertical,
      itemCount: caseSubTypeText.length,
      itemBuilder: (context, index) {
        final map = caseSubTypeText[index];
        final titleSub = map["title"];
        return GestureDetector(
          onTap: () {
            onClick(titleSub);
            /*  //hide keyboard
              FocusScope.of(context).requestFocus(FocusNode());
              setState(() {
                isShowSubItem ? isShowSubItem = false : isShowSubItem = true;
              });

              setState(() {
                caseSubTypeText = titleSub;
              });*/
          },
          child: Card(
            color: Colors.grey[300],
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 10, bottom: 10),
              child: new Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 5,
                  ),
                  Expanded(
                    flex: 5,
                    child: Container(
                      // width: getH(context),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Txt(
                          txt: titleSub,
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.left,
                          maxLines: 4,
                          isBold: false,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    )),
  );
}
