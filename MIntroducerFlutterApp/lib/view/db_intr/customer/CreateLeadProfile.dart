import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/ServerIntr.dart';
import 'package:aitl/controller/helper/db_cus/tab_more/settings/EditProfileHelper.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl_pkg/classes/Common.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../config/db_intro/intro_cfg.dart';
import '../../../controller/api/db_cus/more/settings/EditProfileAPIMgr.dart';
import '../../../controller/api/db_cus/more/settings/GetProfileAPIMgr.dart';
import '../../../controller/form_validator/UserProfileVal.dart';
import '../../../controller/network/NetworkMgr.dart';
import '../../../model/json/auth/UserModel.dart';
import '../../../model/json/db_intr/createlead/ResolutionModel.dart';
import '../../../model/json/db_intr/createlead/email/SendInvitationEmailAPIModel.dart';
import '../../../view_model/api/api_view_model.dart';
import '../../widgets/dialog/DatePickerView.dart';
import '../../widgets/dropdown/DropDownListDialog.dart';
import '../../widgets/input/InputTitleBox.dart';
import '../../widgets/txt/Txt.dart';
import 'CreateLeadCase.dart';

class CreateNewCustomer extends StatefulWidget {
  final bool isNewCaseApplicant;
  final bool isEditCustomer;
  final bool isNewCase;
  final ResolutionModel resolutions;
  const CreateNewCustomer(
      {Key key,
      @required this.resolutions,
      this.isNewCaseApplicant = false,
      this.isNewCase = false,
      this.isEditCustomer = false})
      : super(key: key);

  @override
  _CreateNewCustomerState createState() => _CreateNewCustomerState();
}

enum smokerEnum { yes, no }

class _CreateNewCustomerState extends State<CreateNewCustomer> with Mixin {
  EditProfileHelper editProfileHelper;

  //  personal info
  final _fname = TextEditingController();
  final _mname = TextEditingController();
  final _lname = TextEditingController();
  final _pwd = TextEditingController();
  final _mobile = TextEditingController();
  final _email = TextEditingController();
  final _tel = TextEditingController();
  final _nationalInsuranceNumber = TextEditingController();
  final _refSource = TextEditingController();
  final _passport = TextEditingController();
  //
  final focusFname = FocusNode();
  final focusMname = FocusNode();
  final focusLname = FocusNode();
  final focusEmail = FocusNode();
  final focusMobile = FocusNode();
  final focusTel = FocusNode();
  final focusPwd = FocusNode();
  final focusNIN = FocusNode();
  final focusPP = FocusNode();
  final focusRef = FocusNode();

  //  DOB
  String dob = "";

  //  Passport
  String passportExp = "";
  String visaExp = "";

  smokerEnum _radiosmoker = smokerEnum.no;

  bool isLoading = true;
  bool isSendInvitationEmail = false;

  ResolutionModel resolutions;
  UserModel user;

  updateProfileAPI() async {
    try {
      if (validate()) {
        var param = {};

        if (!widget.isNewCaseApplicant) {
          param = EditProfileHelper().getParam(
            firstName: _fname.text.trim(),
            lastName: _lname.text.trim(),
            email: _email.text.trim(),
            userName: user != null ? user.userName : "",
            profileImageId: user != null ? user.profileImageID.toString() : '0',
            coverImageId: "",
            referenceId: user != null ? user.referenceID.toString() : "",
            referenceType: "",
            //_ref.text.trim(),
            stanfordWorkplaceURL: _refSource.text.toString().trim(),
            //_ref.text.trim(),
            remarks: user != null ? user.remarks : "",
            cohort: editProfileHelper.optGender.title.trim(),
            communityId: user != null ? user.communityID.toString() : "1",
            isFirstLogin: false,
            mobileNumber: _mobile.text.trim(),
            dateofBirth: dob,
            middleName: _mname.text.trim(),
            namePrefix: editProfileHelper.optTitle.title.trim(),
            areYouASmoker: (_radiosmoker == smokerEnum.no) ? 'No' : 'Yes',
            countryCode: '',
            addressLine1: user != null ? user.addressLine1 : "",
            addressLine2: user != null ? user.addressLine2 : "",
            addressLine3: user != null ? user.addressLine3 : "",
            town: user != null ? user.town : "",
            county: user != null ? user.county : "",
            postcode: user != null ? user.postcode : "",
            telNumber: _tel.text.trim(),
            nationalInsuranceNumber: _nationalInsuranceNumber.text.trim(),
            nationality: editProfileHelper.optNationalities.title,
            countryofBirth: editProfileHelper.optCountriesBirth.title,
            countryofResidency: editProfileHelper.optCountriesResidential.title,
            passportNumber: _passport.text.trim(),
            maritalStatus: editProfileHelper.optMaritalStatus.title,
            occupantType: user != null ? user.occupantType : "",
            livingDate: user != null ? user.livingDate : "",
            password: _pwd.text.trim(),
            userCompanyId: widget.isNewCase
                ? userData.userModel.userCompanyID
                : user != null
                    ? user.userCompanyID
                    : resolutions.userCompanyId,
            passportExpiryDate: passportExp,
            visaName: editProfileHelper.optVisaStatus.title,
            visaExpiryDate: visaExp,
            otherVisaName: user != null ? user.otherVisaName : "",
            id: user != null ? user.id : 0,
          );
        } else {
          param = {
            "ProfileImageUrl": "",
            "CommunityId": 1,
            "Title": editProfileHelper.optTitle.title.trim(),
            "FirstName": _fname.text.trim(),
            "MiddleName": _mname.text.trim(),
            "LastName": _lname.text.trim(),
            "Email": _email.text.trim(),
            "MobileNumber": _mobile.text.trim(),
            "TelNumber": "",
            "Postcode": "",
            "DateofBirth": dob,
            "DateofBirthDD": "",
            "DateofBirthMM": "",
            "DateofBirthYY": "",
            "Cohort": editProfileHelper.optGender.title.trim(),
            "Address": "",
            "Address1": "",
            "Town": "",
            "County": editProfileHelper.optCountriesResidential.title,
            "NationalInsuranceNumber": "",
            "Nationality": editProfileHelper.optNationalities.title,
            "CountryofResidency":
                editProfileHelper.optCountriesResidential.title,
            "VisaExpiryDateDD": "",
            "VisaExpiryDateMM": "",
            "VisaExpiryDateYY": "",
            "VisaExpiryDate":
                visaExp == '' ? DateTime.now().toString() : visaExp,
            "CountryofBirth": editProfileHelper.optCountriesBirth.title,
            "PassportNumber": _passport.text.trim(),
            "PassportExpiryDateDD": "",
            "PassportExpiryDateMM": "",
            "PassportExpiryDateYY": "",
            "PassportExpiryDate":
                passportExp == '' ? DateTime.now().toString() : passportExp,
            "MaritalStatus": editProfileHelper.optMaritalStatus.title,
            "Headline": "",
            "BriefBio": "",
            "StanfordWorkplaceURL": "",
            "WorkHistory": "",
            "IsSendingWelcomeEmail": false,
            "AreYouASmoker": (_radiosmoker == smokerEnum.no) ? 'No' : 'Yes',
            "IsSendingInvitationEmail": true,
            "UserCompanyId": widget.isNewCase
                ? userData.userModel.userCompanyID
                : resolutions.userCompanyId
          };
        }

        EditProfileAPIMgr().wsUpdateProfileAPI2(
          context: context,
          param: param,
          isPost: widget.isNewCaseApplicant,
          callback: (model) async {
            if (model != null && mounted) {
              try {
                if (model.success) {
                  try {
                    user = model.responseData.user;

                    if (isSendInvitationEmail) {
                      var url = ServerIntr.GET_INVITATION_EMAIL_URL;
                      url = url.replaceAll("#userId#", user.id.toString());
                      url = url.replaceAll(
                          "#userCompanyId#", user.userCompanyID.toString());
                      APIViewModel().req<SendInvitationEmailAPIModel>(
                          context: context,
                          url: url,
                          reqType: ReqType.Get,
                          callback: (model2) async {
                            print(model2.toString());
                          });
                    }

                    if (widget.isNewCaseApplicant || widget.isNewCase) {
                      Get.back(result: user);
                    } else {
                      Get.off(() =>
                          CreateLeadCase(resolutions: resolutions, user: user));
                    }
                    /*Get.to(
                    () => WebScreen(
                          title: caseModel.title,
                          url: CaseDetailsWebHelper().getLink(
                            title: caseModel.title,
                            taskId: caseModel.id,
                          ),
                          caseID: caseModel.id.toString(),
                        ),
                   );*/
                    /*final msg = model.messages.post_user[0].toString();
                    showToast(context: context, msg: msg, which: 1);
                    Future.delayed(Duration(seconds: 2), () {
                      if (mounted) Get.back();
                    });*/
                  } catch (e) {
                    myLog(e.toString());
                  }
                } else {
                  try {
                    if (mounted) {
                      final err = model.errorMessages.post_user[0].toString();
                      showToast(context: context, msg: err);
                    }
                  } catch (e) {
                    myLog(e.toString());
                  }
                }
              } catch (e) {
                myLog(e.toString());
              }
            }
          },
        );
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    editProfileHelper = null;
    resolutions = null;
    user = null;

    //  personal info
    _pwd.dispose();
    _fname.dispose();
    _mname.dispose();
    _lname.dispose();
    _email.dispose();
    _mobile.dispose();
    _tel.dispose();
    _nationalInsuranceNumber.dispose();
    _refSource.dispose();
    _passport.dispose();

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      resolutions = widget.resolutions;
      editProfileHelper = EditProfileHelper();

      if (!widget.isNewCaseApplicant) {
        GetProfileAPIMgr().wsGetLeadProfileAPI(
          context: context,
          id: widget.isNewCase ? userData.userModel.id : resolutions.userId,
          callback: (model) async {
            if (model != null && mounted) {
              try {
                if (model.success) {
                  try {
                    user = model.responseData.user;

                    await editProfileHelper.getCountriesBirth(
                        context: context, cap: user.countryofBirth);
                    await editProfileHelper.getCountriesResidential(
                        context: context, cap: user.countryofResidency);
                    await editProfileHelper.getCountriesNationaity(
                        context: context, cap: user.nationality);

                    //  personal info
                    _email.text = user.email;
                    _fname.text = user.firstName;
                    _mname.text = user.middleName;
                    _lname.text = user.lastName;
                    _mobile.text =
                        Common.stripCountryCodePhone(user.mobileNumber);
                    _tel.text = Common.stripCountryCodePhone(user.telNumber);

                    //  visa info
                    _nationalInsuranceNumber.text =
                        user.nationalInsuranceNumber;
                    _refSource.text = user.stanfordWorkplaceURL;
                    _passport.text = user.passportNumber;
                    if (user.passportExpiryDate == "0001-01-01T00:00:00" ||
                        user.passportExpiryDate == "1970-01-01T00:00:00") {
                      passportExp = '';
                    } else {
                      passportExp = DateFormat('dd-MMM-yyyy')
                          .format(DateTime.parse(user.passportExpiryDate));
                    }

                    _radiosmoker = (user.areYouASmoker == "No")
                        ? smokerEnum.no
                        : smokerEnum.yes;

                    dob = user.dateofBirth;

                    //  set data into drop down
                    if (user.namePrefix != '') {
                      editProfileHelper.optTitle.id = "1";
                      editProfileHelper.optTitle.title = user.namePrefix;
                    }

                    if (user.cohort != '') {
                      editProfileHelper.optGender.id = "1";
                      editProfileHelper.optGender.title = user.cohort;
                    }

                    if (user.maritalStatus != '') {
                      editProfileHelper.optMaritalStatus.id = "1";
                      editProfileHelper.optMaritalStatus.title =
                          user.maritalStatus;
                    }

                    if (user.countryofBirth != '') {
                      editProfileHelper.optCountriesBirth.id = "1";
                      editProfileHelper.optCountriesBirth.title =
                          user.countryofBirth;
                    }

                    if (user.countryofResidency != '') {
                      editProfileHelper.optCountriesResidential.id = "1";
                      editProfileHelper.optCountriesResidential.title =
                          user.countryofResidency;
                    }

                    if (user.nationality != '') {
                      editProfileHelper.optNationalities.id = "1";
                      editProfileHelper.optNationalities.title =
                          user.nationality;
                    }
                    setState(() {
                      isLoading = false;
                    });
                  } catch (e) {
                    myLog("Error UserInfo data parse = " + e.toString());
                    setState(() {
                      isLoading = false;
                    });
                  }
                } else {
                  setState(() {
                    isLoading = false;
                  });

                  try {
                    if (mounted) {
                      final err = model.errorMessages.post_user[0].toString();
                      showToast(context: context, msg: err);
                    }
                  } catch (e) {
                    myLog(e.toString());
                  }
                }
              } catch (e) {
                myLog(e.toString());
              }
            }
          },
        );
      } else {
        await editProfileHelper.getCountriesBirth(
            context: context, cap: "United Kingdom");
        await editProfileHelper.getCountriesResidential(
            context: context, cap: "United Kingdom");
        await editProfileHelper.getCountriesNationaity(
            context: context, cap: "British");

        setState(() {
          isLoading = false;
        });
      }
    } catch (e) {
      myLog("Error UserInfo data parse = " + e.toString());
    }
  }

  validate() {
    if (!UserProfileVal().isFNameOK(context, _fname)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(context, _lname)) {
      return false;
    } else if (editProfileHelper.optGender.id == null &&
        !widget.isNewCaseApplicant) {
      showToast(context: context, msg: "Please choose gender from the list");
      return false;
    } else if (dob == "" && !widget.isNewCaseApplicant) {
      showToast(context: context, msg: "Please select date of birth");
      return false;
    } else if (!UserProfileVal()
        .isEmailOK(context, _email, "Invalid email address")) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(context, _mobile)) {
      return false;
    } else if (editProfileHelper.optNationalities.id == null &&
        !widget.isNewCaseApplicant) {
      showToast(context: context, msg: "Please choose nationality");
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            backgroundColor: MyTheme.bgColor2,
            //resizeToAvoidBottomPadding: true,
            appBar: AppBar(
              automaticallyImplyLeading: false,
              elevation: 1,
              backgroundColor: MyTheme.titleColor,
              iconTheme:
                  IconThemeData(color: Colors.white //change your color here
                      ),
              leading: (widget.isNewCaseApplicant)
                  ? IconButton(
                      onPressed: () {
                        Get.back(result: user);
                      },
                      icon: Icon(Icons.arrow_back, color: Colors.white))
                  : IconButton(
                      onPressed: () {
                        Get.back();
                      },
                      icon: Icon(Icons.arrow_back, color: Colors.white)),
              title: Txt(
                  txt: widget.isNewCaseApplicant
                      ? "Add Applicant"
                      : widget.isEditCustomer
                          ? "Edit Customer"
                          : "Add Customer",
                  txtColor: Colors.white,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: true),
              centerTitle: true,
            ),
            body: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout())));
  }

  drawLayout() {
    return Container(
      child: (isLoading)
          ? SizedBox()
          : Padding(
              padding: const EdgeInsets.only(left: 30, right: 30),
              child: ListView(
                shrinkWrap: true,
                children: [
                  //SizedBox(height: 20),
                  //drawCamPicker(),
                  //drawHeader(),
                  SizedBox(height: 10),
                  drawPersonalInfoView(),
                  SizedBox(height: 40),
                  userData.userModel.communityID !=
                          IntroCfg.INTRO_COMMUNITYID.toString()
                      ? drawVisaInfoView()
                      : SizedBox(),
                  //drawSendInvitationEmail(),
                  //SizedBox(height: 40),
                  GestureDetector(
                    onTap: () {
                      updateProfileAPI();
                    },
                    child: Container(
                      width: getW(context),
                      height: getHP(context, 7),
                      alignment: Alignment.center,
                      //color: MyTheme.brandColor,
                      decoration: new BoxDecoration(
                        color: MyTheme.brandColor,
                        borderRadius: new BorderRadius.circular(10),
                      ),
                      child: Txt(
                        txt: widget.isNewCaseApplicant
                            ? "Submit"
                            : "Save & Create Case",
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: false,
                      ),
                    ),
                  ),
                  SizedBox(height: 50),
                ],
              ),
            ),
    );
  }

  drawPersonalInfoView() {
    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);

    return Container(
      //width: getW(context),
      child: Column(
        children: [
          /*     Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
            child: DropDownPicker(
              cap: "Choose Title",
              itemSelected: editProfileHelper.optTitle,
              dropListModel: editProfileHelper.ddTitle,
              onOptionSelected: (optionItem) {
                debugPrint("Option Title = ${optionItem}");
                editProfileHelper.optTitle = optionItem;
                setState(() {});
              },
            ),
          ),*/
          userData.userModel.communityID !=
                  IntroCfg.INTRO_COMMUNITYID.toString()
              ? Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Container(
                    width: getW(context),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Flexible(
                              child: Txt(
                                  txt: "Personal Information",
                                  txtColor: MyTheme.brandColor,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.start,
                                  isBold: true),
                            ),
                            SizedBox(width: 5),
                            Container(
                              child: Image.asset(
                                  "assets/images/ico/checked_blue_ico.png"),
                            ),
                          ],
                        ),
                        SizedBox(height: 10),
                        Container(color: Colors.black, height: 0.5)
                      ],
                    ),
                  ),
                )
              : SizedBox(),
          SizedBox(height: 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  child: Txt(
                txt: "Choose Title",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .2,
                isBold: true,
                txtAlign: TextAlign.start,
              )),
              SizedBox(height: 10.0),
              DropDownListDialog(
                context: context,
                title: editProfileHelper.optTitle.title,
                ddTitleList: editProfileHelper.ddTitle,
                callback: (optionItem) {
                  editProfileHelper.optTitle = optionItem;
                  setState(() {});
                },
              ),
            ],
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "First Name",
            input: _fname,
            kbType: TextInputType.name,
            inputAction: TextInputAction.next,
            focusNode: focusFname,
            focusNodeNext: focusMname,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Middle Name (if any)",
            input: _mname,
            kbType: TextInputType.name,
            inputAction: TextInputAction.next,
            focusNode: focusMname,
            focusNodeNext: focusLname,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Last Name",
            input: _lname,
            kbType: TextInputType.name,
            inputAction: TextInputAction.next,
            focusNode: focusLname,
            focusNodeNext: focusEmail,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          /*         Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: DropDownPicker(
              cap: "Choose Gender",
              itemSelected: editProfileHelper.optGender,
              dropListModel: editProfileHelper.ddGender,
              onOptionSelected: (optionItem) {
                editProfileHelper.optGender = optionItem;
                setState(() {});
              },
            ),
          ),*/
          SizedBox(height: 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  width: getW(context),
                  child: Txt(
                    txt: "Choose Gender",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .2,
                    isBold: false,
                    fontWeight: FontWeight.bold,
                    txtAlign: TextAlign.start,
                  )),
              SizedBox(height: 10),
              DropDownListDialog(
                context: context,
                title: editProfileHelper.optGender.title,
                ddTitleList: editProfileHelper.ddGender,
                callback: (optionItem) {
                  editProfileHelper.optGender = optionItem;
                  setState(() {});
                },
              ),
            ],
          ),
          SizedBox(height: 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  width: getW(context),
                  child: Txt(
                    txt: "Choose Marital Status",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .2,
                    isBold: false,
                    fontWeight: FontWeight.bold,
                    txtAlign: TextAlign.start,
                  )),
              SizedBox(height: 10),
              DropDownListDialog(
                context: context,
                title: editProfileHelper.optMaritalStatus.title,
                ddTitleList: editProfileHelper.ddMaritalStatus,
                callback: (optionItem) {
                  editProfileHelper.optMaritalStatus = optionItem;
                  setState(() {});
                },
              ),
            ],
          ),
          SizedBox(height: 20),
          DatePickerView(
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
            cap: 'Select date of birth',
            dt: (dob == '') ? 'Select date of birth' : dob,
            initialDate: dateDOBlast,
            firstDate: dateDOBfirst,
            lastDate: dateDOBlast,
            callback: (value) {
              if (mounted) {
                setState(() {
                  try {
                    dob = DateFormat('dd-MM-yyyy').format(value).toString();
                  } catch (e) {
                    myLog(e.toString());
                  }
                });
              }
            },
          ),
          /*         Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
            child: DropDownPicker(
              cap: "Country of birth",
              itemSelected: editProfileHelper.optCountriesBirth,
              dropListModel: editProfileHelper.ddMaritalCountriesBirth,
              onOptionSelected: (optionItem) {
                editProfileHelper.optCountriesBirth = optionItem;
                setState(() {});
              },
            ),
          ),*/
          SizedBox(height: 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  width: getW(context),
                  child: Txt(
                    txt: "Country of birth",
                    txtColor: Colors.black,
                    fontWeight: FontWeight.bold,
                    txtSize: MyTheme.txtSize - .2,
                    isBold: false,
                    txtAlign: TextAlign.start,
                  )),
              SizedBox(height: 10),
              DropDownListDialog(
                context: context,
                title: editProfileHelper.optCountriesBirth.title,
                ddTitleList: editProfileHelper.ddMaritalCountriesBirth,
                callback: (optionItem) {
                  editProfileHelper.optCountriesBirth = optionItem;
                  setState(() {});
                },
              ),
            ],
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Email",
            input: _email,
            kbType: TextInputType.emailAddress,
            inputAction: TextInputAction.next,
            focusNode: focusEmail,
            focusNodeNext: focusMobile,
            len: 50,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Mobile Number",
            input: _mobile,
            kbType: TextInputType.phone,
            inputAction: TextInputAction.next,
            focusNode: focusMobile,
            focusNodeNext: focusTel,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Telephone Number",
            input: _tel,
            kbType: TextInputType.phone,
            inputAction: TextInputAction.next,
            focusNode: focusTel,
            focusNodeNext: focusNIN,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          userData.userModel.communityID !=
                  IntroCfg.INTRO_COMMUNITYID.toString()
              ? Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: drawInputBox(
                    context: context,
                    title: "National insurance Number",
                    input: _nationalInsuranceNumber,
                    kbType: TextInputType.text,
                    inputAction: TextInputAction.next,
                    focusNode: focusNIN,
                    len: 20,
                    txtColor: Colors.black,
                    fontWeight: FontWeight.bold,
                  ))
              : SizedBox(),
          /*   Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: DropDownPicker(
              cap: "Country of Residence",
              itemSelected: editProfileHelper.optCountriesResidential,
              dropListModel: editProfileHelper.ddMaritalCountriesResidential,
              onOptionSelected: (optionItem) {
                editProfileHelper.optCountriesResidential = optionItem;
                setState(() {});
              },
            ),
          ),*/
          SizedBox(height: 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  width: getW(context),
                  child: Txt(
                    txt: "Country of Residence",
                    txtColor: Colors.black,
                    fontWeight: FontWeight.bold,
                    txtSize: MyTheme.txtSize - .2,
                    isBold: false,
                    txtAlign: TextAlign.start,
                  )),
              SizedBox(height: 10),
              DropDownListDialog(
                context: context,
                title: editProfileHelper.optCountriesResidential.title,
                ddTitleList: editProfileHelper.ddMaritalCountriesResidential,
                callback: (optionItem) {
                  editProfileHelper.optCountriesResidential = optionItem;
                  setState(() {});
                },
              ),
            ],
          ),
          SizedBox(height: 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  width: getW(context),
                  child: Txt(
                    txt: "Nationality",
                    txtColor: Colors.black,
                    fontWeight: FontWeight.bold,
                    txtSize: MyTheme.txtSize - .2,
                    isBold: false,
                    txtAlign: TextAlign.start,
                  )),
              SizedBox(height: 10),
              DropDownListDialog(
                context: context,
                title: editProfileHelper.optNationalities.title,
                ddTitleList: editProfileHelper.ddMaritalNationalities,
                callback: (optionItem) {
                  editProfileHelper.optNationalities = optionItem;
                  setState(() {});
                },
              ),
            ],
          ),
          userData.userModel.communityID !=
                  IntroCfg.INTRO_COMMUNITYID.toString()
              ? drawsmoker()
              : SizedBox(),
        ],
      ),
    );
  }

  drawVisaInfoView() {
    final DateTime dateNow = DateTime.now();
    final dateExplast = DateTime(dateNow.year + 50, dateNow.month, dateNow.day);
    final dateExpfirst = DateTime(dateNow.year, dateNow.month + 1, dateNow.day);
    return Container(
      //width: getW(context),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: getW(context),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Txt(
                          txt: "Visa Information",
                          txtColor: MyTheme.brandColor,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: true),
                    ),
                    SizedBox(width: 5),
                    Container(
                      child:
                          Image.asset("assets/images/ico/checked_blue_ico.png"),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Container(color: Colors.black, height: 0.5)
              ],
            ),
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Passport Number",
            input: _passport,
            kbType: TextInputType.name,
            inputAction: TextInputAction.next,
            focusNode: focusPP,
            focusNodeNext: focusRef,
            len: 50,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 20),
          DatePickerView(
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
            cap: 'Passport Expiry Date',
            dt: (passportExp == '') ? 'Passport Expiry Date' : passportExp,
            initialDate: dateExpfirst,
            firstDate: dateExpfirst,
            lastDate: dateExplast,
            callback: (value) {
              if (mounted) {
                setState(() {
                  try {
                    passportExp =
                        DateFormat('dd-MMM-yyyy').format(value).toString();
                  } catch (e) {
                    myLog(e.toString());
                  }
                });
              }
            },
          ),
          SizedBox(height: 20),
          Txt(
            txt: "Visa Status",
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
            txtSize: MyTheme.txtSize - .2,
            isBold: false,
            txtAlign: TextAlign.start,
          ),
          SizedBox(height: 10),
          DropDownListDialog(
            context: context,
            title: editProfileHelper.optVisaStatus.title,
            ddTitleList: editProfileHelper.ddVisaStatus,
            callback: (optionItem) {
              editProfileHelper.optVisaStatus = optionItem;
              setState(() {});
            },
          ),
          SizedBox(height: 20),
          Txt(
            txt: "Visa Expiry",
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
            txtSize: MyTheme.txtSize - .2,
            isBold: false,
            txtAlign: TextAlign.start,
          ),
          SizedBox(height: 10),
          DatePickerView(
            cap: null,
            dt: (visaExp == '') ? 'Visa Expiry Date' : visaExp,
            initialDate: dateExpfirst,
            firstDate: dateExpfirst,
            lastDate: dateExplast,
            callback: (value) {
              if (mounted) {
                setState(() {
                  try {
                    visaExp = DateFormat('dd-MM-yyyy').format(value).toString();
                  } catch (e) {
                    myLog(e.toString());
                  }
                });
              }
            },
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Reference Source",
            input: _refSource,
            kbType: TextInputType.text,
            inputAction: TextInputAction.done,
            focusNode: focusRef,
            len: 255,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ],
      ),
    );
  }

  drawsmoker() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, top: 20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
                txt: "Are you a smoker?",
                txtColor: Colors.black,
                fontWeight: FontWeight.bold,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 5),
            Theme(
              data: MyTheme.radioThemeData,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _radiosmoker = smokerEnum.no;
                        });
                      }
                    },
                    child: Radio(
                      visualDensity: VisualDensity(horizontal: 0, vertical: 0),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      value: smokerEnum.no,
                      groupValue: _radiosmoker,
                      onChanged: (smokerEnum value) {
                        if (mounted) {
                          setState(() {
                            _radiosmoker = value;
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "No",
                    txtColor: Colors.black,
                    fontWeight: FontWeight.w400,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                  SizedBox(width: 20),
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          _radiosmoker = smokerEnum.yes;
                        });
                      }
                    },
                    child: Radio(
                      visualDensity: VisualDensity(horizontal: 0, vertical: 0),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      value: smokerEnum.yes,
                      groupValue: _radiosmoker,
                      onChanged: (smokerEnum value) {
                        if (mounted) {
                          setState(() {
                            _radiosmoker = value;
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "Yes",
                    txtColor: Colors.black,
                    fontWeight: FontWeight.w400,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  drawSendInvitationEmail() {
    return widget.isNewCaseApplicant
        ? Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                    width: 20,
                    height: 20,
                    child: Container(
                        margin: const EdgeInsets.all(0),
                        padding: const EdgeInsets.all(0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border:
                              Border.all(color: MyTheme.brandColor, width: 1),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Checkbox(
                            splashRadius: 20,
                            fillColor:
                                MaterialStateProperty.all(Colors.transparent),
                            focusColor: MyTheme.brandColor,
                            checkColor: MyTheme.brandColor,
                            value: isSendInvitationEmail,
                            onChanged: (value) {
                              setState(() {
                                isSendInvitationEmail = value;
                              });
                            }))),
                SizedBox(width: 10),
                Txt(
                  txt: "Send Invitation Email",
                  txtColor: Colors.black,
                  fontWeight: FontWeight.w400,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
              ],
            ),
          )
        : SizedBox();
  }
}
