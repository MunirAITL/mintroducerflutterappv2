import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../widgets/btn/MMBtn.dart';
import '../../widgets/dialog/DatePickerView.dart';
import '../../widgets/dropdown/DropDownPicker.dart';
import '../../widgets/txt/Txt.dart';
import 'package:intl/intl.dart';

enum eSearch {
  Stage,
  Status,
  DueDate,
}

abstract class BaseLead<T extends StatefulWidget> extends State<T> with Mixin {
  refreshData();
  doSearch(eSearch v);

  final searchByTxt = TextEditingController();

  final DateTime dateNow = DateTime.now();

  var date1 = "";
  var date2 = "";

  var datelast;
  var datefirst;
  var datelast2;
  var datefirst2;

  double wBox;

  //  list dropdown case owner
  DropListModel ddCaseOwner = DropListModel([]);
  OptionItem optCaseOwner = OptionItem(id: null, title: "Select case owner");

  String selectedStage = "Select stage";
  final List<String> listStage = [
    "Select stage",
    "Added",
    "Qualifying",
    "Processing",
    "Converted",
    "Junk",
    "None"
  ];

  String selectedStatus = "All";
  final List<String> listStatus = [
    "All",
    "Converted to customer",
    "Call back later",
    "Attempt to contact",
    "Needs information",
    "Wait for now",
    "Unsolicited call",
    "Do not contact again",
    "Not found property yet",
    "No longer proceeding",
    "Changed circumstances",
    "Wrong number",
    "Gone elsewhere",
    "Lost to competition",
    "Deleted"
  ];

  String selectedSortBy = "Select order";
  final List<String> listSortBy = [
    "Select order",
    "Last udate date",
    "My last call",
    "My last email",
    "My last text",
    "Last creation date",
    "Name"
  ];

  drawNF() {
    return Container(
      width: getW(context),
      //height: getH(context) / 2,
      //color: Colors.black,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        //shrinkWrap: true,
        children: [
          SizedBox(height: 20),
          Container(
            width: getWP(context, 50),
            height: getWP(context, 30),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                    "assets/images/screens/db_cus/my_cases/case_nf2.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          //SizedBox(height: 40),
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
            child: Container(
              child: Txt(
                txt: "Manage listing not found.\nTry with other options",
                txtColor: MyTheme.mycasesNFBtnColor,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false,
                //txtLineSpace: 1.5,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> drawTimePicker(TimeOfDay t, Function(TimeOfDay) callback) async {
    final picked_s = await showTimePicker(
        context: context,
        initialTime: t,
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light().copyWith(
              colorScheme: ColorScheme.light(
                // change the border color
                primary: MyTheme.brandColor,
                // change the text color
                onSurface: Colors.purple,
              ),
              // button colors
              buttonTheme: ButtonThemeData(
                colorScheme: ColorScheme.light(
                  primary: Colors.green,
                ),
              ),
            ),
            child: child,
          );
        });
    if (picked_s != null && picked_s != t) {
      callback(picked_s);
    }
  }

  drawCustomDate() {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
              child: _drawCusDateRow(
                  "From", date1, datefirst, datelast, datefirst, (DateTime d) {
            setState(() {
              date1 = DateFormat('dd-MMM-yyyy').format(d).toString();
            });
          })),
          SizedBox(width: 10),
          Flexible(
              child: _drawCusDateRow(
                  "To", date2, datefirst2, datelast2, DateTime.now(), (d) {
            setState(() {
              date2 = DateFormat('dd-MMM-yyyy').format(d).toString();
            });
          })),
        ],
      ),
    );
  }

  _drawCusDateRow(String title, String dLabel, fdate, ldate, initialDate,
      Function(DateTime) callback) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          //width: getWP(context, 25),
          decoration: BoxDecoration(
              color: Colors.grey.shade200,
              border: Border.all(
                color: Colors.black26,
              ),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(5),
                bottomLeft: Radius.circular(5),
              )),
          child: Padding(
            padding: const EdgeInsets.all(8),
            child: Txt(
                txt: title,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .5,
                txtAlign: TextAlign.center,
                isBold: false),
          ),
        ),
        Expanded(
          flex: 3,
          child: GestureDetector(
            onTap: () {
              showDatePicker(
                context: context,
                initialDate: initialDate,
                firstDate: fdate,
                lastDate: ldate,
                fieldHintText: "dd/mm/yyyy",
                builder: (context, child) {
                  return Theme(
                    data: ThemeData.light().copyWith(
                      colorScheme: ColorScheme.light(
                        // change the border color
                        primary: MyTheme.brandColor,
                        // change the text color
                        onSurface: Colors.black,
                      ),
                      // button colors
                      buttonTheme: ButtonThemeData(
                        colorScheme: ColorScheme.light(
                          primary: Colors.green,
                        ),
                      ),
                    ),
                    child: child,
                  );
                },
              ).then((value) {
                if (value != null) {
                  callback(value);
                }
              });
            },
            child: Container(
              width: getW(context),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: .5),
                  borderRadius: BorderRadius.all(Radius.circular(0))),
              child: Padding(
                padding: const EdgeInsets.only(left: 15, top: 10, bottom: 10),
                child: Text(
                  dLabel,
                  style: TextStyle(color: Colors.black, fontSize: 13),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
