import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/ServerIntr.dart';
import 'package:aitl/config/db_intro/intro_cfg.dart';
import 'package:aitl/controller/api/db_intr/leadfetch/LeadCaseAPIMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/view/db_intr/createlead/LeadBase.dart';
import 'package:aitl/view/db_intr/createlead/LeadDetails.dart';
import 'package:aitl/view/widgets/dropdown/DropDownButton2.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog2.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl_pkg/classes/DateFun.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../../controller/network/NetworkMgr.dart';
import '../../../controller/observer/StateListnerIntro.dart';
import '../../../model/json/db_intr/createlead/CaseOwnerAPIModel.dart';
import '../../../model/json/db_intr/createlead/GetAllResSupportAdminAPIModel.dart';
import '../../../model/json/db_intr/createlead/GetTwilioPhoneNumberAPIModel.dart';
import '../../../model/json/db_intr/createlead/GetUserTaskCatAPIModel.dart';
import '../../../model/json/db_intr/createlead/ResolutionModel.dart';
import '../../../view_model/api/api_view_model.dart';
import '../../../view_model/helper/ui_helper.dart';
import '../../widgets/dropdown/DropDownPicker.dart';
import '../../widgets/images/MyNetworkImage.dart';
import '../../widgets/progress/AppbarBotProgbar.dart';
import '../../widgets/txt/Txt.dart';
import 'leaddetails/create_lead.dart';

class LeadsScreen extends StatefulWidget {
  const LeadsScreen({Key key}) : super(key: key);

  @override
  State createState() => _LeadsScreenState();
}

class _LeadsScreenState extends BaseLead<LeadsScreen> with StateListenerIntro {
  StateProviderIntro _stateProvider;

  List<ResolutionModel> listResolutions = [];
  List<dynamic> listAllSupAdminResolutions = [];
  List<UserTaskCategorys> userTaskCategorys = [];
  List<Users> listOwnerName;
  String twilioPhoneNumber = '';

  int pageStart = 1;
  int pageCount = AppConfig.page_limit;
  bool isPageDone = false;
  bool isLoading = false;

  @override
  onStateChanged(ObserverStateIntro state) async {
    if (state == ObserverStateIntro.STATE_CHANGED_tabbar2) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (mounted) resetDate();
      });
    }
  }

  caseOwnerAPI() async {
    try {
      var url = ServerIntr
          .GET_USERBYCOMMUNITYID_COMPUSERID_FOR_SELECT_OPT_ADVISOR_URL;
      //url = url.replaceAll("#communityId#", userData.userModel.communityID);  //  BS consultant
      //url = url.replaceAll(
      //"#userCompanyId#", userData.userModel.userCompanyID.toString());  //  BS consultant
      url = url.replaceAll(
          "#userId#", userData.userModel.id.toString()); //  MM negotiator
      await APIViewModel().req<CaseOwnerAPIModel>(
          context: context,
          url: url,
          reqType: ReqType.Get,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                listOwnerName = model.responseData.users;
                ddCaseOwner.listOptionItems
                    .add(OptionItem(id: null, title: "Select case owner"));
                for (final caseOwner in listOwnerName) {
                  if (IntroCfg.listCaseOwnerCommunityID
                          .contains(caseOwner.communityId) &&
                      caseOwner.name != null) {
                    ddCaseOwner.listOptionItems.add(OptionItem(
                        id: caseOwner.id.toString(), title: caseOwner.name));
                  }
                }
              }
            }
          });
    } catch (e) {}
  }

  getUserTaskCategoryAPI() async {
    try {
      await APIViewModel().req<GetUserTaskCatAPIModel>(
          context: context,
          url: ServerIntr.GET_USER_TASK_CATEGORY_URL,
          reqType: ReqType.Get,
          isLoading: false,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                userTaskCategorys = model.responseData.userTaskCategorys;
              }
            }
          });
    } catch (e) {}
  }

  geTwilioPhoneNumberAPI() async {
    try {
      await APIViewModel().req<GetTwilioPhoneNumberAPIModel>(
          context: context,
          url: ServerIntr.GET_TWILIO_PHONENUMBER_URL
              .replaceAll("#userId#", userData.userModel.id.toString()),
          reqType: ReqType.Get,
          isLoading: false,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                twilioPhoneNumber = model.responseData.twilioPhoneNumber;
              }
            }
          });
    } catch (e) {}
  }

  geAllResSupAdminIDAPI() async {
    try {
      var url = ServerIntr.GET_ALL_RES_SUPPORT_ADMINID_URL;
      url =
          url.replaceAll("#SupportAdminId#", userData.userModel.id.toString());
      url = url.replaceAll("#IsLockAutoCall#", "1");
      await APIViewModel().req<GetAllResSupportAdminAPIModel>(
          context: context,
          url: url,
          reqType: ReqType.Get,
          isLoading: false,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                listAllSupAdminResolutions = model.responseData.resolutions;
              }
            }
          });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    listResolutions = null;
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    super.dispose();
  }

  @override
  doSearch(eSearch v) {
    switch (v) {
      case eSearch.Stage:
        break;
      case eSearch.Status:
        break;
      case eSearch.DueDate:
        break;
      default:
    }
    refreshData();
  }

  appInit() async {
    try {
      _stateProvider = new StateProviderIntro();
      _stateProvider.subscribe(this);
      resetDate();
      WidgetsBinding.instance.addPostFrameCallback((_) {
        // executes after build
        wBox = getW(context) / 3.5;
      });
      await caseOwnerAPI();
      await refreshData();
      await getUserTaskCategoryAPI();
      await geTwilioPhoneNumberAPI();
      await geAllResSupAdminIDAPI();
      setState(() {});
    } catch (e) {}
  }

  resetDate() {
    datelast = DateTime(dateNow.year, dateNow.month, dateNow.day);
    datefirst = DateTime(dateNow.year - 10, dateNow.month, dateNow.day - 7);
    datelast2 = DateTime(dateNow.year, dateNow.month, dateNow.day);
    datefirst2 = DateTime(dateNow.year - 10, dateNow.month, dateNow.day);
    date1 = DateFormat('dd-MMM-yyyy')
        .format(DateTime.now().subtract(new Duration(days: 7)));
    date2 = DateFormat('dd-MMM-yyyy').format(DateTime.now());
  }

  Future<void> refreshData() async {
    setState(() {
      pageStart = 1;
      isPageDone = false;
      isLoading = true;
      listResolutions.clear();
    });
    onLazyLoadAPI();
  }

  onLazyLoadAPI() async {
    if (mounted) {
      setState(() {
        isLoading = true;
      });
      await LeadCaseAPIMgr().getLeadCase(
          context: context,
          param: {
            'UserCompanyId': userData.userModel.userCompanyID.toString(),
            'Criteria': '1',
            'AssigneeId': optCaseOwner.id ?? "0",
            'Status': selectedStatus == "All" ? "" : selectedStatus,
            'IsSpecificDate': "",
            'FromDateTime': date1,
            'ToDateTime': date2,
            'InitiatorId': userData.userModel.id.toString(),
            'Title': '',
            'Stage': selectedStage != "Select stage" ? selectedStage : '',
            'LeadStatus': '',
            'Page': pageStart.toString(),
            'Count': pageCount.toString(),
            'SearchText': searchByTxt.text.trim(),
            'OrderBy': selectedSortBy != 'Select order' ? selectedSortBy : '',
            'ResolutionType': "All",
            'DateTimeSelectOption': ''
          },
          callback: (model) {
            if (mounted && model != null) {
              if (model.success) {
                final List<ResolutionModel> resolutions =
                    model.responseData.resolutions;
                if (resolutions != null) {
                  //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                  if (resolutions.length != pageCount) {
                    isPageDone = true;
                  }
                  try {
                    for (ResolutionModel res in resolutions) {
                      listResolutions.add(res);
                    }
                  } catch (e) {
                    myLog(e.toString());
                  }

                  setState(() {
                    isLoading = false;
                  });
                } else {
                  setState(() {
                    isLoading = false;
                  });
                }
              } else {
                listResolutions = [];
              }
              setState(() {});
            }
          });
    }
    if (mounted) {
      setState(() {
        isLoading = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          elevation: 1,
          backgroundColor: MyTheme.titleColor,
          title: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                UIHelper().drawAppbarTitle(title: "Manage leads"),
                Spacer(),
                listOwnerName != null
                    ? GestureDetector(
                        onTap: () {
                          Get.to(() => CreateLeadPage(
                              ddCaseOwner: ddCaseOwner,
                              optCaseOwner: optCaseOwner)).then((value) {
                            refreshData();
                          });
                        },
                        child: Icon(Icons.add_outlined, color: Colors.white),
                      )
                    : SizedBox(),
              ]),
          centerTitle: false,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight((isLoading) ? .5 : 0),
            child: (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox(),
          ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return NotificationListener(
      onNotification: (scrollNotification) {
        if (scrollNotification is ScrollStartNotification) {
          //print('Widget has started scrolling');
        } else if (scrollNotification is ScrollEndNotification) {
          if (!isPageDone && listResolutions.length > 0) {
            pageStart++;
            onLazyLoadAPI();
          }
        }
        return true;
      },
      child: SingleChildScrollView(
        primary: true,
        child: Column(
          children: [
            drawSearchHeader(),
            (listResolutions.length > 0)
                ? ListView.builder(
                    addAutomaticKeepAlives: true,
                    cacheExtent: AppConfig.page_limit.toDouble(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    primary: false,
                    itemCount: listResolutions.length,
                    itemBuilder: (BuildContext context, int index) {
                      return drawItems(listResolutions[index]);
                    },
                  )
                : (isLoading)
                    ? CircularProgressIndicator()
                    : drawNF(),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  drawItems(ResolutionModel resolutions) {
    var stageColor = MyTheme.titleColor;
    try {
      stageColor = IntroCfg.stageColor[resolutions.stage];
    } catch (e) {}

    final name = (resolutions.firstName ?? '') +
        " " +
        (resolutions.middleName ?? '') +
        " " +
        (resolutions.lastName ?? '');

    return GestureDetector(
      onTap: () {
        Get.to(() => LeadDetails(
              resolution: resolutions,
              listOwnerName: listOwnerName,
            )).then((value) => refreshData());
      },
      child: (resolutions != null)
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(5.0),
                  child: Container(
                    //height: 100.0,
                    margin: const EdgeInsets.only(
                        bottom: 6.0), //Same as `blurRadius` i guess
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: MyTheme.purpleColor,
                          offset: Offset(0.0, 0.5), //(x,y)
                          blurRadius: .5,
                        ),
                      ],
                    ),

                    child: IntrinsicHeight(
                      child: Row(
                        children: [
                          Container(
                              width: getWP(context, .5), color: stageColor),
                          SizedBox(width: 10),
                          Container(
                            width: getWP(context, 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(height: 10),
                                Container(
                                  width: getWP(context, 12),
                                  height: getWP(context, 12),
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: MyNetworkImage.loadProfileImage(
                                          resolutions.profileImageUrl),
                                      fit: BoxFit.cover,
                                    ),
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                SizedBox(height: 5),
                                Container(
                                  decoration: BoxDecoration(
                                    color: Colors.grey.shade300,
                                    borderRadius: BorderRadius.circular(20),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.all(2),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        SizedBox(width: 5),
                                        Txt(
                                            txt: "Stage",
                                            txtColor: Colors.black,
                                            txtSize: MyTheme.txtSize - .6,
                                            txtAlign: TextAlign.start,
                                            isBold: false),
                                        drawCircle(
                                            context: context,
                                            color: stageColor,
                                            size: 2.5),
                                        SizedBox(width: 2),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(height: 5),
                              ],
                            ),
                          ),
                          SizedBox(width: 10),
                          Expanded(
                            child: Container(
                              width: getWP(context, 79),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(height: 10),
                                  Txt(
                                      txt: name ?? '',
                                      txtColor: Colors.black,
                                      txtSize: MyTheme.txtSize - .4,
                                      txtAlign: TextAlign.start,
                                      isBold: true),
                                  SizedBox(height: 1),
                                  Txt(
                                      txt: resolutions.resolutionType ?? '',
                                      txtColor: Colors.black,
                                      txtSize: MyTheme.txtSize - .4,
                                      txtAlign: TextAlign.start,
                                      isBold: true),
                                  SizedBox(height: 1),
                                  Txt(
                                      txt: resolutions.title ?? '',
                                      txtColor: Colors.red,
                                      txtSize: MyTheme.txtSize - .4,
                                      txtAlign: TextAlign.start,
                                      isBold: false),
                                  SizedBox(height: 1),
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.email,
                                        color: Colors.grey,
                                        size: 15,
                                      ),
                                      SizedBox(width: 5),
                                      Flexible(
                                        child: Txt(
                                            txt: resolutions.emailAddress ?? '',
                                            txtColor: Colors.black,
                                            txtSize: MyTheme.txtSize - .4,
                                            txtAlign: TextAlign.start,
                                            isBold: false),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 2),
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.phone,
                                        color: Colors.grey,
                                        size: 15,
                                      ),
                                      SizedBox(width: 5),
                                      Flexible(
                                        child: Txt(
                                            txt: resolutions.phoneNumber ?? '',
                                            txtColor: Colors.black,
                                            txtSize: MyTheme.txtSize - .4,
                                            txtAlign: TextAlign.start,
                                            isBold: false),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 2),
                                  Row(
                                    children: [
                                      Flexible(
                                        child: Txt(
                                            txt: resolutions.stage ?? '',
                                            txtColor: Colors.black,
                                            txtSize: MyTheme.txtSize - .5,
                                            txtAlign: TextAlign.start,
                                            isBold: false),
                                      ),
                                      SizedBox(width: 5),
                                      Icon(Icons.watch_later_rounded,
                                          color: Colors.grey.shade400,
                                          size: 20),
                                      SizedBox(width: 5),
                                      Flexible(
                                        child: Txt(
                                            txt: DateFun.getDate(
                                                    resolutions.creationDate,
                                                    "kk:mm, dd MMM yyyy") ??
                                                '',
                                            txtColor: Colors.black,
                                            txtSize: MyTheme.txtSize - .6,
                                            txtAlign: TextAlign.start,
                                            isBold: false),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 10),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            )
          : SizedBox(),
    );
  }

  drawSearchHeader() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10, top: 10),
      child: Container(
        width: getW(context),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            drawDDList(
                context: context,
                title: "Stage",
                items: listStage,
                indexHint: 0,
                selectedValue: selectedStage,
                callback: (value) {
                  selectedStage = value;
                  setState(() {});
                }),
            SizedBox(height: 5),
            drawDDList(
                context: context,
                title: "Lead status",
                items: listStatus,
                indexHint: 1,
                selectedValue: selectedStatus,
                callback: (value) {
                  selectedStatus = value;
                  setState(() {});
                }),
            SizedBox(height: 5),
            Txt(
                txt: 'Search by Creation Date Range',
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .6,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 2),
            drawCustomDate(),
            /*SizedBox(height: 5),
            drawDDList(
                context: context,
                title: "Sort by",
                items: listSortBy,
                indexHint: 0,
                selectedValue: selectedSortBy,
                callback: (value) {
                  selectedSortBy = value;
                  setState(() {});
                }),*/
            /*SizedBox(height: 5),
            DropDownListDialog2(
              radius: 5,
              context: context,
              title: optCaseOwner.title,
              id: optCaseOwner.id,
              ddTitleList: ddCaseOwner,
              callback: (optionItem) {
                optCaseOwner = optionItem;
                setState(() {});
              },
            ),*/
            /*SizedBox(height: 5),
            SizedBox(
              height: 40,
              child: TextField(
                controller: searchByTxt,
                style: TextStyle(color: Colors.black, fontSize: 14),
                decoration: new InputDecoration(
                  isDense: true,
                  counterText: "",
                  contentPadding: EdgeInsets.zero,
                  prefixIcon: new Icon(Icons.search, color: Colors.grey),
                  suffixIcon: GestureDetector(
                      onTap: () {
                        searchByTxt.clear();
                      },
                      child: Icon(
                        Icons.close,
                        color: Colors.black,
                      )),
                  hintText: "Lead id/Name/Email/Phone Number etc",
                  hintStyle: TextStyle(color: Colors.grey, fontSize: 14),
                  enabledBorder: const OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    borderSide: const BorderSide(
                      color: Colors.grey,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),
            ),*/
            Padding(
              padding: const EdgeInsets.only(left: 40, right: 40),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                      child: OutlinedButton(
                          onPressed: () {
                            optCaseOwner = OptionItem(
                                id: null, title: "Select case owner");
                            selectedStage = "Select stage";
                            selectedStatus = "All";
                            selectedSortBy = "Select order";
                            searchByTxt.clear();
                            resetDate();
                            refreshData();
                          },
                          style: OutlinedButton.styleFrom(
                            backgroundColor:
                                MyTheme.purpleColor.withOpacity(.9),
                            side: BorderSide(width: 1, color: Colors.white),
                          ),
                          child: Text(
                            "Refresh",
                            style: TextStyle(color: Colors.white),
                          ))),
                  Flexible(
                    child: OutlinedButton(
                      onPressed: () {
                        refreshData();
                      },
                      style: OutlinedButton.styleFrom(
                        backgroundColor: MyTheme.purpleColor,
                        side: BorderSide(width: 1, color: Colors.white),
                      ),
                      child: const Text(
                        "Search",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
