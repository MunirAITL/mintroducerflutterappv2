import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/material.dart';

import '../widgets/txt/Txt.dart';

mixin IntroMixin {
  radioButtonitem(
      {BuildContext context,
      String text,
      double txtSize = 2,
      String bgColor,
      Color textColor}) {
    return Container(
      width: MediaQuery.of(context).size.width / 2,
      padding: EdgeInsets.only(left: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border(
              left: BorderSide(color: Colors.grey, width: 1),
              right: BorderSide(color: Colors.grey, width: 1),
              top: BorderSide(color: Colors.grey, width: 1),
              bottom: BorderSide(color: Colors.grey, width: 1)),
          color: HexColor.fromHex(bgColor)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 20,
            height: 20,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                border: Border(
                    left: BorderSide(color: Colors.grey, width: 1),
                    right: BorderSide(color: Colors.grey, width: 1),
                    top: BorderSide(color: Colors.grey, width: 1),
                    bottom: BorderSide(color: Colors.grey, width: 1)),
                color: Colors.white),
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    border: Border(
                        left: BorderSide(color: Colors.grey, width: 1),
                        right: BorderSide(color: Colors.grey, width: 1),
                        top: BorderSide(color: Colors.grey, width: 1),
                        bottom: BorderSide(color: Colors.grey, width: 1)),
                    color: Colors.grey),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Txt(
                  txt: text,
                  txtColor: textColor,
                  txtSize: txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
          ),
        ],
      ),
    );
  }

  radioButtonitem2(
      {BuildContext context,
      String text,
      double txtSize = 1.5,
      Color bgColor,
      Color textColor}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          width: 15,
          height: 15,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              border: Border(
                  left: BorderSide(color: Colors.grey, width: 1),
                  right: BorderSide(color: Colors.grey, width: 1),
                  top: BorderSide(color: Colors.grey, width: 1),
                  bottom: BorderSide(color: Colors.grey, width: 1)),
              color: bgColor),
          child: Padding(
            padding: const EdgeInsets.all(3),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  border: Border(
                      left: BorderSide(color: Colors.grey, width: 1),
                      right: BorderSide(color: Colors.grey, width: 1),
                      top: BorderSide(color: Colors.grey, width: 1),
                      bottom: BorderSide(color: Colors.grey, width: 1)),
                  color: Colors.white),
            ),
          ),
        ),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(5),
            child: Txt(
                txt: text,
                txtColor: textColor,
                txtSize: txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
        ),
      ],
    );
  }

  inputFieldWithText(
      {BuildContext context,
      TextEditingController controller,
      String titleText,
      String hintText,
      var keyBoardType}) {
    if (keyBoardType == null) {
      keyBoardType = TextInputType.text;
    }
    return Container(
      //width: getW(context),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10,
          ),
          Txt(
              txt: titleText,
              txtColor: MyTheme.inputColor,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.start,
              isBold: false),
          SizedBox(height: 10),
          Container(
            //width: getW(context),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border(
                    left: BorderSide(color: Colors.grey, width: 1),
                    right: BorderSide(color: Colors.grey, width: 1),
                    top: BorderSide(color: Colors.grey, width: 1),
                    bottom: BorderSide(color: Colors.grey, width: 1)),
                color: Colors.white),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: new TextFormField(
                    controller: controller,
                    textInputAction: TextInputAction.next,
                    decoration: new InputDecoration(
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      hintText: hintText,
                      counterText: "",
                      hintStyle: TextStyle(color: Colors.grey),
                      fillColor: Colors.black,
                    ),
                    validator: (val) {
                      if (val.length == 0) {
                        return "Name cannot be empty";
                      } else {
                        return null;
                      }
                    },
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: MyTheme.txtSize + 16,
                    ),
                    keyboardType: keyBoardType,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
