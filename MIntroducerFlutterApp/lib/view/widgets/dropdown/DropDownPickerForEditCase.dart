import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';
//  https://stackoverflow.com/questions/49273157/how-to-implement-drop-down-list-in-flutter

class DropDownPickerForEditCase extends StatefulWidget {
  final String cap;
  final OptionItemSelectNewCaseEdit itemSelected;
  final DropListModel dropListModel;
  final Color bgColor;
  Color txtColor;
  double txtSize;
  double ddTitleSize;
  double ddRadius;
  final bool isFullWidth;
  List selectedItemList = [];

  final Function(OptionItemSelectNewCaseEdit optionItem) onOptionSelected;

  DropDownPickerForEditCase({
    @required this.cap,
    @required this.itemSelected,
    @required this.dropListModel,
    @required this.onOptionSelected,
    this.bgColor = Colors.transparent,
    this.txtColor = Colors.black,
    this.txtSize,
    this.ddTitleSize = 0,
    this.ddRadius = 10,
    this.isFullWidth = false,
  }) {
    if (txtSize == null) {
      this.txtSize = MyTheme.txtSize;
    }
  }

  @override
  State createState() => _DropDownPickerState(itemSelected, dropListModel);
}

class _DropDownPickerState extends State<DropDownPickerForEditCase>
    with SingleTickerProviderStateMixin, Mixin {
  OptionItemSelectNewCaseEdit optionItemSelected;
  final DropListModel dropListModel;
  _DropDownPickerState(this.optionItemSelected, this.dropListModel);

  AnimationController expandController;
  Animation<double> animation;

  bool isShow = false;

  @override
  void initState() {
    super.initState();
    expandController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 0));
    animation = CurvedAnimation(
      parent: expandController,
      curve: Curves.fastOutSlowIn,
    );
    _runExpandCheck();
  }

  void _runExpandCheck() {
    if (isShow) {
      expandController.forward();
    } else {
      expandController.reverse();
    }
  }

  @override
  void dispose() {
    expandController.dispose();
    super.dispose();
  }

  //@override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        this.isShow = !this.isShow;
        _runExpandCheck();
        setState(() {});
      },
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            (widget.cap != null)
                ? Txt(
                    txt: widget.cap,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: true)
                : SizedBox(),
            SizedBox(height: (widget.cap != null) ? 15 : 0),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 5),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(widget.ddRadius),
                  border: Border(
                      left: BorderSide(color: Colors.grey, width: 1),
                      right: BorderSide(color: Colors.grey, width: 1),
                      top: BorderSide(color: Colors.grey, width: 1),
                      bottom: BorderSide(color: Colors.grey, width: 1)),
                  color: widget.bgColor),
              child: new Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Txt(
                              txt: optionItemSelected.title,
                              txtColor: widget.txtColor,
                              txtSize: MyTheme.txtSize - widget.ddTitleSize,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          optionItemSelected.subTitle.isNotEmpty
                              ? Txt(
                                  txt: "-> " + optionItemSelected.subTitle,
                                  txtColor: widget.txtColor,
                                  txtSize: MyTheme.txtSize - .5,
                                  txtAlign: TextAlign.start,
                                  isBold: false)
                              : SizedBox(),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment(1, 0),
                    child: Icon(
                      isShow
                          ? Icons.keyboard_arrow_up
                          : Icons.keyboard_arrow_down,
                      color: Colors.black,
                      size: 40,
                    ),
                  ),
                ],
              ),
            ),
            SizeTransition(
                axisAlignment: 1.0,
                sizeFactor: animation,
                child: Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Container(
                      child: ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    //padding: const EdgeInsets.only(top: 40, bottom: 40),
                    scrollDirection: Axis.vertical,
                    itemCount: dropListModel.listOptionItems.length,
                    itemBuilder: (context, index) {
                      final map = NewCaseCfg.listCreateNewCase[index];
                      final icon = map["url"];
                      final title = map["title"];
                      List subTitleList = map["subItem"];
                      return Card(
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                          ),
                          child: ListTileTheme(
                            tileColor: MyTheme.lGrayColor,
                            contentPadding: EdgeInsets.only(right: 0),
                            child: ExpansionTile(
                              trailing: SizedBox.shrink(),
                              initiallyExpanded: false,
                              title: Padding(
                                padding: const EdgeInsets.only(
                                    top: 10.0, bottom: 10.0),
                                child: new Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      width: 7,
                                    ),
                                    Flexible(
                                      child: Container(
                                        width: getWP(context, 12),
                                        height: getWP(context, 12),
                                        //color: Colors.black,
                                        child: Image.asset(
                                          icon,
                                          //fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 5,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            width: getH(context),
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 15.0),
                                              child: Txt(
                                                txt: title,
                                                txtColor: Colors.black,
                                                txtSize: MyTheme.txtSize,
                                                txtAlign: TextAlign.left,
                                                maxLines: 4,
                                                isBold: false,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Flexible(
                                      child: subTitleList.isEmpty
                                          ? Container(
                                              child: Icon(
                                              widget.selectedItemList
                                                      .contains(index)
                                                  ? Icons.arrow_forward
                                                  : Icons.arrow_forward,
                                              size: 25.0,
                                              color: Colors.black,
                                            ))
                                          : Container(
                                              //color: Colors.black,
                                              child: Icon(
                                              widget.selectedItemList
                                                      .contains(index)
                                                  ? Icons.arrow_drop_up
                                                  : Icons.arrow_drop_down,
                                              size: 35.0,
                                              color: Colors.black,
                                            )),
                                    ),
                                  ],
                                ),
                              ),
                              onExpansionChanged: (value) {
                                if (value && subTitleList.isNotEmpty) {
                                  setState(() {
                                    widget.selectedItemList.add(index);
                                  });
                                } else {
                                  if (widget.selectedItemList.contains(index)) {
                                    setState(() {
                                      widget.selectedItemList.remove(index);
                                    });
                                  }
                                }

                                print(
                                    "value = $value and subtitle = ${widget.selectedItemList}");

                                if (subTitleList.isEmpty) {
                                  this.optionItemSelected =
                                      new OptionItemSelectNewCaseEdit(
                                          id: dropListModel
                                              .listOptionItems[index].id,
                                          title: dropListModel
                                              .listOptionItems[index].title,
                                          subId: "",
                                          subTitle: "");
                                  isShow = false;
                                  expandController.reverse();
                                  widget.onOptionSelected(
                                      this.optionItemSelected);
                                }
                              },
                              children: [
                                ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: subTitleList.length,
                                  itemBuilder: (context, subIndex) {
                                    final subTitle =
                                        subTitleList[subIndex]["title"];
                                    return GestureDetector(
                                      onTap: () {
                                        this.optionItemSelected =
                                            new OptionItemSelectNewCaseEdit(
                                                id: dropListModel
                                                    .listOptionItems[index].id,
                                                title: dropListModel
                                                    .listOptionItems[index]
                                                    .title,
                                                subId: subTitleList[subIndex]
                                                        ["index"]
                                                    .toString(),
                                                subTitle: subTitle);
                                        isShow = false;
                                        expandController.reverse();
                                        widget.onOptionSelected(
                                            this.optionItemSelected);
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            left: 18.0, top: 10, bottom: 10),
                                        child: Container(
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Container(
                                                    width: 5,
                                                    height: 5,
                                                    color: Colors.black,
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  Container(
                                                    child: Txt(
                                                      txt: subTitle,
                                                      txtColor: Colors.black,
                                                      txtSize: MyTheme.txtSize,
                                                      txtAlign: TextAlign.start,
                                                      isBold: true,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(
                                                height: 15,
                                              ),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(right: 5),
                                                width: getWP(context, 100),
                                                height: 1,
                                                color: Colors.black,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  )),

                  /* child: _buildDropListOptions(
                          dropListModel.listOptionItems, context)),*/
                )),
//          Divider(color: Colors.grey.shade300, height: 1,)
          ],
        ),
      ),
    );
  }
/*
  Column _buildDropListOptions(List<OptionItem> items, BuildContext context) {
    int i = 0;
    int len = items.length;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children:items.map((item) => _buildSubMenu(item, context, len, i++)).toList(),
    );
  }

  Widget _buildSubMenu( OptionItem item, BuildContext context, int len, int index) {
    return GestureDetector(
      child: Container(
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Txt(
                  txt: item.title,
                  txtColor: Colors.black,
                  txtSize: widget.txtSize - .4,
                  txtAlign: TextAlign.start,
                  isBold: false,
                  isOverflow: true,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Container(color: Colors.grey, height: .5),
              )
            ],
          ),
        ),
      ),
      onTap: () {
        this.optionItemSelected = item;
        isShow = false;
        expandController.reverse();
        widget.onOptionSelected(item);
      },
    );
  }*/
}
