import 'package:aitl/config/MyTheme.dart';
import 'package:flutter/material.dart';
import '../txt/Txt.dart';
import 'package:dropdown_button2/dropdown_button2.dart';

drawDDList({
  @required BuildContext context,
  @required String title,
  int indexHint = 0,
  @required List<String> items,
  @required dynamic selectedValue,
  @required Function(dynamic) callback,
}) {
  double width = MediaQuery.of(context).size.width;
  return Row(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Container(
        width: width * .25,
        decoration: BoxDecoration(
          color: Colors.grey.shade200,
          border: Border(
            left: BorderSide(color: Colors.grey, width: .5),
            top: BorderSide(color: Colors.grey, width: .5),
            bottom: BorderSide(color: Colors.grey, width: .5),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(9.5),
          child: Txt(
              txt: title,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .4,
              txtAlign: TextAlign.center,
              isBold: false),
        ),
      ),
      //SizedBox(width: 10),
      Expanded(
        child: DropdownButtonHideUnderline(
          child: DropdownButton2(
            buttonHeight: 40,
            //buttonWidth: width,
            buttonPadding: const EdgeInsets.only(left: 14, right: 14),
            buttonDecoration: BoxDecoration(
              //borderRadius: BorderRadius.circular(0),
              border: Border(
                left: BorderSide(color: Colors.grey, width: .5),
                right: BorderSide(color: Colors.grey, width: .5),
                top: BorderSide(color: Colors.grey, width: .5),
                bottom: BorderSide(color: Colors.grey, width: .5),
              ),
              color: MyTheme.bgColor2,
            ),
            dropdownDecoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: MyTheme.bgColor2,
            ),
            iconEnabledColor: Colors.grey,
            iconDisabledColor: Colors.grey,
            hint: Text(
              items[indexHint],
              style: TextStyle(
                fontSize: 14,
                color: Colors.black,
              ),
            ),
            items: items
                .map((item) => DropdownMenuItem<String>(
                      value: item,
                      child: Text(
                        item,
                        style: const TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                        ),
                      ),
                    ))
                .toList(),
            value: selectedValue,
            onChanged: (value) {
              return callback(value);
            },
            //itemHeight: 40,
          ),
        ),
      ),
    ],
  );
}
