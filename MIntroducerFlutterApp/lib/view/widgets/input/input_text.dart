import 'package:flutter/material.dart';

import '../../../config/MyTheme.dart';
import '../txt/Txt.dart';

inputFieldWithText(
    {BuildContext context,
    TextEditingController controller,
    String titleText,
    String hintText,
    var keyBoardType}) {
  if (keyBoardType == null) {
    keyBoardType = TextInputType.text;
  }
  return Container(
    //width: getW(context),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 10,
        ),
        Txt(
            txt: titleText,
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.start,
            isBold: true),
        SizedBox(
          height: 10,
        ),
        Container(
          //width: getW(context),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              border: Border(
                  left: BorderSide(color: Colors.grey, width: 1),
                  right: BorderSide(color: Colors.grey, width: 1),
                  top: BorderSide(color: Colors.grey, width: 1),
                  bottom: BorderSide(color: Colors.grey, width: 1)),
              color: Colors.white),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                child: new TextFormField(
                  controller: controller,
                  textInputAction: TextInputAction.next,
                  decoration: new InputDecoration(
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    hintText: hintText,
                    counterText: "",
                    hintStyle: TextStyle(color: Colors.grey),
                    fillColor: Colors.black,
                  ),
                  validator: (val) {
                    if (val.length == 0) {
                      return "Name cannot be empty";
                    } else {
                      return null;
                    }
                  },
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: MyTheme.txtSize + 16,
                  ),
                  keyboardType: keyBoardType,
                ),
              ),
            ],
          ),
        )
      ],
    ),
  );
}
