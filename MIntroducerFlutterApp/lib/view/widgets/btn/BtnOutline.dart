import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';

class BtnOutline extends StatelessWidget with Mixin {
  final String txt;
  final Color txtColor;
  final Color borderColor;
  final Function callback;
  double radius;
  BtnOutline({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.borderColor,
    @required this.callback,
    this.radius = 10,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: MaterialButton(
        child: new Txt(
            txt: txt,
            txtColor: txtColor,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.start,
            isBold: false),

        onPressed: () {
          callback();
        },
        //borderSide: BorderSide(color: borderColor),
        shape: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(radius),
          borderSide: BorderSide(
              style: BorderStyle.solid, width: .7, color: borderColor),
        ),

        //color: Colors.black,
      ),
    );
  }
}
