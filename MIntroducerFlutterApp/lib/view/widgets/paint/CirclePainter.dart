import 'dart:ui';
import 'package:aitl/config/db_cus/HomeCfg.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;
//  https://medium.com/flutter-community/playing-with-paths-in-flutter-97198ba046c8

class CirclePainter extends CustomPainter {
  final int total;
  final double red;
  final double lYellow;
  final double yellow;
  final double lGreen;
  final double width;
  final double height;
  final double fontSize;

  CirclePainter({
    @required this.total,
    @required this.red,
    @required this.lYellow,
    @required this.yellow,
    @required this.lGreen,
    @required this.width,
    @required this.height,
    @required this.fontSize,
  });

  final TextPainter textPainter = TextPainter(
    textDirection: TextDirection.ltr,
  );

  @override
  void paint(Canvas canvas, Size size) {
    //double totalLeadPA = 100 - (completePA + fmaPA + offeredPA);

    double borderWidth = 18;

    Paint redPaint = new Paint()
      ..color = HomeCfg.red
      ..strokeCap = StrokeCap.square
      ..style = PaintingStyle.stroke
      ..strokeWidth = borderWidth;

    Paint lYellowPaint = new Paint()
      ..color = HomeCfg.lYellow
      ..strokeCap = StrokeCap.square
      ..style = PaintingStyle.stroke
      ..strokeWidth = borderWidth;

    Paint yellowPaint = new Paint()
      ..color = HomeCfg.yellow
      ..strokeCap = StrokeCap.square
      ..style = PaintingStyle.stroke
      ..strokeWidth = borderWidth;

    Paint lGreenPaint = new Paint()
      ..color = HomeCfg.lGreen
      ..strokeCap = StrokeCap.square
      ..style = PaintingStyle.stroke
      ..strokeWidth = borderWidth;

    Paint greenPaint = new Paint()
      ..color = HomeCfg.green
      ..strokeCap = StrokeCap.square
      ..style = PaintingStyle.stroke
      ..strokeWidth = borderWidth;

    Offset center = new Offset(size.width / 2, size.height / 2);
    double radius = math.min(size.width / 2, size.height / 2);
    canvas.drawCircle(center, radius, greenPaint);

    //  red = completed
    double arcAngle = 2 * math.pi * (red / 100);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius),
        -math.pi / 2, arcAngle, false, redPaint);

    //  red light = fma completed
    double arcAngle2 = 2 * math.pi * (lYellow / 100);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius),
        -(math.pi / 2) + arcAngle, arcAngle2, false, lYellowPaint);

    double arcAngle3 = 2 * math.pi * (yellow / 100);
    canvas.drawArc(new Rect.fromCircle(center: center, radius: radius),
        -(math.pi / 2) + arcAngle + arcAngle2, arcAngle3, false, yellowPaint);

    double arcAngle4 = 2 * math.pi * (lGreen / 100);
    canvas.drawArc(
        new Rect.fromCircle(center: center, radius: radius),
        -(math.pi / 2) + arcAngle + arcAngle2 + arcAngle3,
        arcAngle4,
        false,
        lGreenPaint);

    drawText(arcAngle, canvas, size, total.toString());

    //_drawDashedLine(canvas, size);
  }

  void drawText(double pos, Canvas canvas, Size size, String text) {
    final style1 = TextStyle(
        color: Colors.black.withAlpha(240),
        fontSize: fontSize * 2,
        letterSpacing: 1.2,
        fontWeight: FontWeight.w900);
    /*final style2 = TextStyle(
        color: Colors.black.withAlpha(240),
        fontSize: fontSize,
        letterSpacing: 1.2,
        fontWeight: FontWeight.w900);*/
    textPainter.text = TextSpan(style: style1, text: text);
    textPainter.layout();
    textPainter.paint(canvas, Offset(width * .12, size.height / 2.5));

    /*textPainter.text = TextSpan(style: style2, text: "Total Cases");
    textPainter.layout();
    textPainter.paint(canvas, Offset(width * .06, size.height / 1.8));*/
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}

class CircleDotPainter extends CustomPainter {
  final int total;

  CircleDotPainter({@required this.total});

  @override
  void paint(Canvas canvas, Size size) {
    Paint innerPaint1 = new Paint()
      ..color = Colors.purple
      ..strokeCap = StrokeCap.butt
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2;
    Paint innerPaint2 = new Paint()
      ..color = Colors.white
      ..strokeCap = StrokeCap.butt
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2;

    int i = 0;
    Offset center = new Offset(size.width / 2, size.height / 2);
    double radius = math.min(size.width / 2, size.height / 2);
    while (i < total) {
      if (i % 2 == 0) {
        drawDashLine(canvas, center, radius, innerPaint1, i, i + 1);
      } else {
        drawDashLine(canvas, center, radius, innerPaint2, i, i + 1);
      }
      i++;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }

  drawDashLine(Canvas canvas, Offset center, double radius, Paint paint,
      int start, int end) {
    double innerAngle1 = 2 * math.pi * (start / 100);
    double innerAngle2 = 2 * math.pi * ((end - start) / 100);
    double innerAngle = (math.pi / 2) - innerAngle1;
    canvas.drawArc(
      new Rect.fromCircle(center: center, radius: radius),
      -innerAngle,
      innerAngle2,
      false,
      paint,
    );
  }
}
