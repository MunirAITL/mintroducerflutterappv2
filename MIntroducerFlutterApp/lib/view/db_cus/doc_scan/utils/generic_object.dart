import 'package:provider/provider.dart';

abstract class Decodable<T> {
  T decode(dynamic data);
}

abstract class GenericObject<T> {
  // define a typedef with Decodable object
  Create<Decodable> create;

  // init object with create param
  GenericObject({this.create});

  T genericObject(context, dynamic data) {
    // get create object
    final item = create(context);
    // now, we can call decode func from Decodable class
    return item.decode(data);
  }
}
