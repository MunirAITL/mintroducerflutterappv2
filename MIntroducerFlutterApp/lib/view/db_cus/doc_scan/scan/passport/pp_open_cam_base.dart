import 'dart:io';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/data/ScanDocData.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/passport/pp_open_cam_page.dart';
import 'package:aitl/view/db_cus/doc_scan/scan/passport/pp_review_page.dart';
import 'package:aitl_pkg/widgets/crop_image.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../../Mixin.dart';

abstract class OpenCamPPBase<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final GlobalKey key = GlobalKey();
  CameraController controller;
  XFile imageFile;

  double _minAvailableExposureOffset = 0.0;
  double _maxAvailableExposureOffset = 0.0;
  double _currentExposureOffset = 0.0;
  double _minAvailableZoom = 1.0;
  double _maxAvailableZoom = 1.0;

  drawLayout();

  void onTakePictureButtonPressed() {
    takePicture().then((XFile file) async {
      if (mounted && file != null) {
        await Get.off(() => CropImagePage(
              file: File(file.path),
              title: "Crop your passport",
              txtColor: Colors.white,
              appbarColor: MyTheme.statusBarColor,
            )).then((file) {
          if (file != null) {
            scanDocData.file_pp_front = file;
            Get.to(() => PPReviewPage()).then((value) {
              if (value != null) {
                Get.off(() => OpenCamPPPage());
              }
            });
          }
        });
      }
    });
  }

  Future<XFile> takePicture() async {
    final CameraController cameraController = controller;
    if (cameraController == null || !cameraController.value.isInitialized) {
      showInSnackBar('Error: select a camera first.');
      return null;
    }

    if (cameraController.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      XFile file = await cameraController.takePicture();
      return file;
    } on CameraException catch (e) {
      showToast(context: context, msg: e.toString());
      return null;
    }
  }

  void showInSnackBar(String message) {
    // ignore: deprecated_member_use
    scaffoldKey.currentState?.showSnackBar(SnackBar(content: Text(message)));
  }

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }

    final CameraController cameraController = CameraController(
      cameraDescription,
      ResolutionPreset.veryHigh,
      //enableAudio: enableAudio,
      imageFormatGroup: ImageFormatGroup.jpeg,
    );

    controller = cameraController;

    // If the controller is updated then update the UI.
    cameraController.addListener(() {
      if (mounted) setState(() {});
      if (cameraController.value.hasError) {
        showInSnackBar(
            'Camera error ${cameraController.value.errorDescription}');
      }
    });

    try {
      await cameraController.initialize();
      await Future.wait([
        cameraController
            .getMinExposureOffset()
            .then((value) => _minAvailableExposureOffset = value),
        cameraController
            .getMaxExposureOffset()
            .then((value) => _maxAvailableExposureOffset = value),
        cameraController
            .getMaxZoomLevel()
            .then((value) => _maxAvailableZoom = value),
        cameraController
            .getMinZoomLevel()
            .then((value) => _minAvailableZoom = value),
      ]);
    } on CameraException catch (e) {
      showToast(context: context, msg: e.toString());
    }

    if (mounted) {
      setState(() {});
    }
  }
}
