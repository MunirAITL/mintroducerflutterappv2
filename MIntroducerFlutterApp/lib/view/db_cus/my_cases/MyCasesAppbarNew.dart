import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';

Widget MyCasesAppbarNew(
    {BuildContext context,
    double h,
    bool isLoading,
    Function callback,
    Function callbackWebView}) {
  return AppBar(
    iconTheme: IconThemeData(color: Colors.white),
    backgroundColor: MyTheme.statusBarColor,
    elevation: MyTheme.appbarElevation,
    title: UIHelper().drawAppbarTitle(title: "My Cases"),
    centerTitle: false,
    bottom: PreferredSize(
      preferredSize: Size.fromHeight((isLoading) ? .5 : 0),
      child: (isLoading)
          ? AppbarBotProgBar(
              backgroundColor: MyTheme.appbarProgColor,
            )
          : SizedBox(),
    ),
    /* bottom: PreferredSize(
      preferredSize: Size.fromHeight(h * 7 / 100),
      child: Column(
        children: [
          Container(
            color: MyTheme.brandColor,
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 20, right: 20, top: 15, bottom: 15),
              child: Center(
                child: Txt(
                    txt: "View the ongoing cases",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
            ),
          ),
          (isLoading)
              ? AppbarBotProgBar(
                  backgroundColor: MyTheme.appbarProgColor,
                )
              : Container()
        ],
      ),
    ),*/
  );
}
