import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/db_cus/tab_more/MoreHelper.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/db_cus/noti/NotiTab.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';

class CalculatorTab extends StatefulWidget {
  @override
  State createState() => _MoreTabState();
}

class _MoreTabState extends State<CalculatorTab> with Mixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  StateProvider _stateProvider = StateProvider();

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _stateProvider = null;
    super.dispose();
  }

  appInit() {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.themeData.accentColor,
          title: UIHelper().drawAppbarTitle(title: "Calculator"),
          centerTitle: true,
          actions: <Widget>[
            SizedBox(width: 30)
            /*   IconButton(
              iconSize: 30,
              icon: Image.asset("assets/images/icons/help_circle_icon.png"),
              onPressed: () {
                // do something
                Get.to(
                  () => WebScreen(
                    title: "Help",
                    url: Server.HELP_INFO_URL,
                  ),
                ).then((value) {
                  //callback(route);
                });
              },
            )*/
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Container(
            child: ListView.builder(
              itemCount: MoreHelper.listMore.length,
              itemBuilder: (context, index) {
                Map<String, dynamic> mapMore = MoreHelper.listMore[index];
                return GestureDetector(
                    onTap: () async {},
                    child: Center(
                        child: Container(
                            child: Txt(
                                txt: "Working on it ",
                                txtColor: Colors.black,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                isBold: true))));
              },
            ),
          ),
        ),
      ),
    );
  }
}
