import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/db_cus/calculator/mortgage_cal_items.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/input/InputTitleBoxMM4.dart';
import 'package:aitl/view/widgets/radio/RadioButtonitem.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/PriceBox.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view_model/rx/mortgage_cal.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MortgageCalPage extends StatefulWidget {
  const MortgageCalPage({Key key}) : super(key: key);

  @override
  State createState() => _MortgageCalPageState();
}

class _MortgageCalPageState extends State<MortgageCalPage> with Mixin {
  final mortgageCalController = Get.put(MortgageCalController());
  final income = TextEditingController();
  final deposit = TextEditingController();
  final termYears = TextEditingController();

  RegExp reg = RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))');
  String Function(Match) mathFunc = (Match match) => '${match[1]},';

  @override
  void initState() {
    super.initState();
    income.text = mortgageCalController.anualIncomeAmount.value
        .toStringAsFixed(0)
        .replaceAllMapped(reg, mathFunc);
    deposit.text = mortgageCalController.depositAmount.value
        .toStringAsFixed(0)
        .replaceAllMapped(reg, mathFunc);
    termYears.text = mortgageCalController.mortgageTerm.value.toString();
    calculate();
  }

  calculate() {
    try {
      mortgageCalController.maxLoanAmount.value =
          mortgageCalController.anualIncomeAmount.value *
              mortgageCalController.mortgageRate.value;
      mortgageCalController.homeAmount.value =
          mortgageCalController.maxLoanAmount.value +
              mortgageCalController.depositAmount.value;
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: 0,
          automaticallyImplyLeading: false,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back)),
          title: UIHelper().drawAppbarTitle(title: "Mortgage Calculator"),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
          child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
              txt: "Enter your details",
              txtColor: MyTheme.dBlueAirColor,
              txtSize: MyTheme.txtSize + .3,
              txtAlign: TextAlign.start,
              isBold: true,
            ),
            SizedBox(height: 20),
            drawNewMortgageRadio(),
            SizedBox(height: 20),
            drawIncomeInput(),
            SizedBox(height: 10),
            drawDepositInput(),
            SizedBox(height: 10),
            drawFixedRadio(),
            SizedBox(height: 20),
            drawPeriodRadio(),
            SizedBox(height: 20),
            drawMortgageTerms(),
            SizedBox(height: 10),
            drawLookingBuy2LetMortgage(),
            SizedBox(height: 40),
            Btn(
                txt: "CALCULATE",
                txtColor: Colors.white,
                bgColor: MyTheme.dBlueAirColor,
                height: getHP(context, 7),
                width: getW(context),
                radius: 5,
                callback: () {
                  Get.to(() => MortgageCalItems());
                }),
            SizedBox(height: 50),
          ],
        ),
      )),
    );
  }

  drawNewMortgageRadio() {
    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: Txt(
                txt: "What are you looking for?",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
            ),
            GestureDetector(
                onTap: () {
                  showToolTips(
                      context: context,
                      txt:
                          "New mortgage: Find your first mortgage, or an additional mortgage for another property.\n\nRemortgage: see if you can get a better rate, or borrow more, on a mortgage you already have.");
                },
                child: Icon(
                  Icons.info_outline_rounded,
                  color: Colors.black54,
                  size: 20,
                ))
          ],
        ),
        //SizedBox(height: 10),
        IntrinsicHeight(
          child: (Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                  child: radioButtonitem(
                      index: 1,
                      text: "New mortgage",
                      bgColor: (mortgageCalController.caseType.value ==
                              "New mortgage")
                          ? MyTheme.dBlueAirColor
                          : Color(0xFFFFF),
                      textColor: (mortgageCalController.caseType.value ==
                              "New mortgage")
                          ? Colors.white
                          : Colors.black,
                      callback: (i) {
                        mortgageCalController.caseType.value = "New mortgage";
                        mortgageCalController.mortgageRate.value = 4.5;
                        calculate();
                        setState(() {});
                      })),
              SizedBox(width: 10),
              Expanded(
                  child: radioButtonitem(
                      index: 0,
                      text: "Remortgage",
                      bgColor:
                          (mortgageCalController.caseType.value == "Remortgage")
                              ? MyTheme.dBlueAirColor
                              : Color(0xFFFFF),
                      textColor:
                          (mortgageCalController.caseType.value == "Remortgage")
                              ? Colors.white
                              : Colors.black,
                      callback: (i) {
                        mortgageCalController.caseType.value = "Remortgage";
                        mortgageCalController.mortgageRate.value = 4.6;
                        calculate();
                        setState(() {});
                      })),
            ],
          )),
        ),
      ],
    ));
  }

  drawIncomeInput() {
    return InputTitleBoxMM4(
      title: "What's your income?",
      ph: "0.0",
      input: income,
      kbType: TextInputType.number,
      len: 11,
      minLen: 0,
      prefixIco: Text(getCurSign(),
          style: TextStyle(color: Colors.white, fontSize: 17)),
      tooltipTxt:
          "If you're applying with someone else, include theirs too. This means all types of income you get in a year: from bonuses, to overtime, to rental income.",
      onChange: (v) {
        if (v != null) {
          try {
            mortgageCalController.anualIncomeAmount.value =
                double.parse(v.toString().replaceAll(",", ""));
          } catch (e) {
            mortgageCalController.anualIncomeAmount.value = 0.0;
          }
          calculate();
        }
      },
    );
  }

  drawDepositInput() {
    return InputTitleBoxMM4(
      title: "How much is your deposit?",
      ph: "0.0",
      input: deposit,
      kbType: TextInputType.number,
      len: 11,
      minLen: 0,
      prefixIco: Text(getCurSign(),
          style: TextStyle(color: Colors.white, fontSize: 17)),
      tooltipTxt: "The amount you have saved for deposit.",
      onChange: (v) {
        if (v != null && v != '') {
          mortgageCalController.depositAmount.value =
              double.parse(v.toString().replaceAll(",", ""));
          calculate();
        }
      },
    );
  }

  drawFixedRadio() {
    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: Txt(
                txt: "Rate type",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
            ),
            GestureDetector(
                onTap: () {
                  showToolTips(
                      context: context,
                      txt:
                          "Fixed rate: your interest rate (which affects what you pay each month) is guaranteed over a fixed period of time, and won't change.\n\nVariable rate: your interest rate could go up or down, so what you pay each month could change. During the initial period, you get a discount on your variable rate.");
                },
                child: Icon(
                  Icons.info_outline_rounded,
                  color: Colors.black54,
                  size: 20,
                ))
          ],
        ),
        //SizedBox(height: 10),
        IntrinsicHeight(
          child: (Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                  child: radioButtonitem(
                      index: 1,
                      text: "Fixed",
                      bgColor: (mortgageCalController.interestRateType.value ==
                              "Fixed")
                          ? MyTheme.dBlueAirColor
                          : Color(0xFFFFF),
                      textColor:
                          (mortgageCalController.interestRateType.value ==
                                  "Fixed")
                              ? Colors.white
                              : Colors.black,
                      callback: (i) {
                        mortgageCalController.interestRateType.value = "Fixed";
                        setState(() {});
                      })),
              SizedBox(width: 10),
              Expanded(
                  child: radioButtonitem(
                      index: 0,
                      text: "Variable",
                      bgColor: (mortgageCalController.interestRateType.value ==
                              "Variable")
                          ? MyTheme.dBlueAirColor
                          : Color(0xFFFFF),
                      textColor:
                          (mortgageCalController.interestRateType.value ==
                                  "Variable")
                              ? Colors.white
                              : Colors.black,
                      callback: (i) {
                        mortgageCalController.interestRateType.value =
                            "Variable";
                        setState(() {});
                      })),
            ],
          )),
        ),
      ],
    ));
  }

  drawPeriodRadio() {
    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: Txt(
                txt: "Initial period (years)",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
            ),
            GestureDetector(
                onTap: () {
                  showToolTips(
                      context: context,
                      txt:
                          "For the first few years of your mortgage, you'll get a lower than standard interest rate. Think of it like an introductory offer from your lender. You can set how long this lasts here.");
                },
                child: Icon(
                  Icons.info_outline_rounded,
                  color: Colors.black54,
                  size: 20,
                ))
          ],
        ),
        //SizedBox(height: 10),
        IntrinsicHeight(
          child: (Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                  child: radioButtonitem(
                      index: 2,
                      text: "2",
                      bgColor: (mortgageCalController.initialPeriod.value == 2)
                          ? MyTheme.dBlueAirColor
                          : Color(0xFFFFF),
                      textColor:
                          (mortgageCalController.initialPeriod.value == 2)
                              ? Colors.white
                              : Colors.black,
                      callback: (i) {
                        mortgageCalController.initialPeriod.value = i;
                        setState(() {});
                      })),
              SizedBox(width: 10),
              Expanded(
                  child: radioButtonitem(
                      index: 3,
                      text: "3",
                      bgColor: (mortgageCalController.initialPeriod.value == 3)
                          ? MyTheme.dBlueAirColor
                          : Color(0xFFFFF),
                      textColor:
                          (mortgageCalController.initialPeriod.value == 3)
                              ? Colors.white
                              : Colors.black,
                      callback: (i) {
                        mortgageCalController.initialPeriod.value = i;
                        setState(() {});
                      })),
              SizedBox(width: 10),
              Expanded(
                  child: radioButtonitem(
                      index: 4,
                      text: "4",
                      bgColor: (mortgageCalController.initialPeriod.value == 4)
                          ? MyTheme.dBlueAirColor
                          : Color(0xFFFFF),
                      textColor:
                          (mortgageCalController.initialPeriod.value == 4)
                              ? Colors.white
                              : Colors.black,
                      callback: (i) {
                        mortgageCalController.initialPeriod.value = i;
                        setState(() {});
                      })),
              SizedBox(width: 10),
              Expanded(
                  child: radioButtonitem(
                      index: 5,
                      text: "5",
                      bgColor: (mortgageCalController.initialPeriod.value == 5)
                          ? MyTheme.dBlueAirColor
                          : Color(0xFFFFF),
                      textColor:
                          (mortgageCalController.initialPeriod.value == 5)
                              ? Colors.white
                              : Colors.black,
                      callback: (i) {
                        mortgageCalController.initialPeriod.value = i;
                        setState(() {});
                      })),
            ],
          )),
        ),
      ],
    ));
  }

  drawMortgageTerms() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InputTitleBoxMM4(
            title: "Mortgage terms",
            ph: "0",
            input: termYears,
            kbType: TextInputType.phone,
            len: 2,
            minLen: 0,
            suffixIco: Text("years",
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 17)),
            tooltipTxt:
                "The total length of your loan - how many years you'll take to pay back your mortgage.",
            onChange: (v) {
              if (v != null && v != '') {
                mortgageCalController.mortgageTerm.value = int.parse(v);
              }
            },
          )
        ],
      ),
    );
  }

  drawLookingBuy2LetMortgage() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Txt(
            txt: "Looking for a buy-to-let mortgage?",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.center,
            isBold: false,
          ),
          SizedBox(height: 10),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              //color: Colors.white,
              border: Border.all(
                color: MyTheme.dBlueAirColor,
                style: BorderStyle.solid,
                width: 1.0,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: Container(
                      //color: Colors.black,
                      child: Txt(
                          txt: "Try our buy-to-let calculator",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.center,
                          isBold: false),
                    ),
                  ),
                  //Spacer(),
                  Icon(
                    Icons.arrow_forward_ios,
                    size: 20,
                    color: Colors.black,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
